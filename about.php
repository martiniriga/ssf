<?php
/**
*Template Name: About 
*/
?>
<?php get_header(); ?>
<div class="who">
		<div class="grid container about">
			<div class="col-1-3">
				<h2>Who we are?</h2>
				<p class="subtitle">
					Sarasa is a research and writing firm that values proffessionalism,intergrity and equity
				</p>
			</div>
		</div>
	</div>

	<div class="about">
		<div class="container">
			<h2>Why choose us?</h2>
			<p class="subtitle">
				Effective research and writing adds value. At Sarasa we believe it influences the "What" and "Who" for our clients. We are an attentive and detail oriented team with the drive to provide what our clients need the most - INSIGHT 
			</p>
		</div>
	</div>

	<div class="about packages">
		<div class="container">
			<h2>Our Packages</h2>
			<p class="subtitle">
				We have tailor made packages for both our individual and corporate clients. For more information contact us.
			</p>
			<div class="grid">
				<div class="col-1-2">
					<h2>Individuals or scholars</h2>
					<p>Students both you and old who need help in research and writing of research papers, developing study tools, articles and proposals among others.</p>
				</div>
				<div class="col-1-2">
					<h2>Corporates and organizations</h2>
					<p>Institutions that need assistance in conducting research, monitoring and evaluation, writing or developing study tools.</p>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>