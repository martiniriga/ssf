<?php get_header();
if($_COOKIE['language']=='somalia'){
	$cat = 'newsletter_so';
}else{
	$cat = 'newsletter';
}
	$month = get_query_var( 'monthnum' );
	$year = get_query_var( 'year' );
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
	  'monthnum' => $month,
		'year'=> $year,
	  'paged' => $paged,
	  'post_type'  => 'post' ,
	  'post_status' => 'publish',
	  'category_name' => $cat,
	  'ignore_sticky_posts'  => true,
	);

	$the_query = new WP_Query( $args );


?>

<div id="content">
	<span id="DeltaPlaceHolderMain">
		<div id="main-news">
			<div class="wrapper-news clearfix">
				<div id="posts-list" class="clearfix">


					<?php if($the_query->have_posts() ): ?>
						<?php $i=0; ?>
						<div class="entry-date"><div class="month"><?php the_time('F'); ?></div> <div class="year"><?php the_time('Y'); ?></div> <em></em> </div>
						<?php while($the_query->have_posts() ): $the_query->the_post(); ?>
							<?php
							$article="" ;
							if($i % 2 == 0){
								$article= "other-articles";
								if($i == 0)
								$article= "first-article";
							}else{
								$article= "other-articles article-odd";
							}
							?>
							<article class="format-standard news-items news-related <?php echo $article; $i++;?>">
								<a href="<?php the_permalink(); ?>"><div class="post-heading"><h4 class="post-title"><?php the_title(); ?></h4></div></a>
								<div class="summary">
									<?php the_excerpt(); ?>
								</div>
							</article>
						<?php endwhile; ?>
						<div class="page-navigation clearfix news-related">
				    	<div class="nav-previous" style="display: block;">
				    	 <?php next_posts_link( '← hoore Warsidaha', $the_query->max_num_pages ); ?>
				    	</div>
				    	<div class="nav-next" style="display: block;">
				        <?php  previous_posts_link('ku xig Warsidaha →' ); ?>
				    	</div>
				    </div>
					<?php else: ?>

						<div id="post-404" class="noposts">

							<p><?php _e('None found.','example'); ?></p>

						</div><!-- /#post-404 -->

					<?php endif;?>

				</div>
				<?php if ( in_category( 'newsletter' ) ) : ?>
					 <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
								<aside id="sidebar" role="complementary">
										<?php dynamic_sidebar( 'sidebar-1' ); ?>
								</aside><!-- .widget-area -->
					<?php endif; ?>
				<?php else : ?>
					<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
							 <aside id="sidebar" role="complementary">
									 <?php dynamic_sidebar( 'sidebar-2' ); ?>
							 </aside><!-- .widget-area -->
				 <?php endif; ?>
				<?php endif; ?>
			</div>
		</span>
	</div><!-- /#content -->

	<?php get_footer(); ?>
