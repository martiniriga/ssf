<?php
get_header();

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
  'date_pagination_type' => 'monthly',
  'paged'                => $paged,
  'post_type'  => 'post' ,
  'post_status' => 'publish',
  'category_name' => 'blog',
  'ignore_sticky_posts'  => true,
);


$the_query = new WP_Query( $args );
?>
<div id="content">
  <span id="DeltaPlaceHolderMain">
    <table cellpadding="0" cellspacing="0" border="0" width="95%">
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" class="ms-blog-MainArea">
            <tr>
              <td valign="top">

                <?php if(have_posts() ): ?>
                  <ul class="ms-blog-postList">
                  <?php while(have_posts() ): the_post(); ?>

                    <li>
                      <div class="ms-blog-postBox ms-shadow">
                        <div class="ms-blog-postBoxMargin">
                          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                          <br>
                          <div class="ms-blog-postBody"><?php the_content(); ?></div>
                        </div>
                    </li>
                <br><br>
                <?php endwhile; ?>
              </ul>
                <?php else: ?>

                <div id="post-404" class="noposts">
                <p><?php _e('None found.','example'); ?></p>
                </div><!-- /#post-404 -->

              <?php endif;?>




              </td>
              <td valign="top" class="ms-blog-LeftColumn"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </span>
</div><!-- /#content -->

<?php get_footer(); ?>