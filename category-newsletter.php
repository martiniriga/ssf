<?php get_header(); ?>
<?php
// Set the paged variable (see: http://codex.wordpress.org/Pagination ).
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

// Example arguments.
$args = array(

    // set the date pagination to 'monthly'
    'date_pagination_type' => 'monthly', // 'yearly', 'monthly', 'daily'
    'paged'                => $paged,

    // Add your own query arguments here
    'post_type'  => 'post' ,
    'post_status' => 'publish',
    'category_name' => 'newsletter',
    'ignore_sticky_posts'  => true,
);

// The custom query.
$the_query = new WP_Query( $args );


// Variable $the_query is the query object.
// Don't forget to add the query object to the new functions when using a custom query;
?>
<div id="content">
<span id="DeltaPlaceHolderMain">
	<div id="main-news">
                <div class="wrapper-news clearfix">
                    <div id="posts-list" class="clearfix">


							<?php if($the_query->have_posts() ): ?>
							<?php $i=0; ?>
							<div class="entry-date"><div class="month"><?php the_time('F'); ?></div> <div class="year"><?php the_time('Y'); ?></div> <em></em> </div>
							<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>
							<?php

								$article="" ;
								if($i % 2 == 0){
									$article= "other-articles";
									if($i == 0)
										$article= "first-article";
								}else{
									$article= "other-articles article-odd";
								}
									?>
								<article class="format-standard news-items news-related <?php echo $article; $i++;?>">
									<a href="<?php the_permalink(); ?>"><div class="post-heading"><h4 class="post-title"><?php the_title(); ?></h4></div></a>
									<div class="summary">
									<?php the_excerpt(); ?>
									</div>
								</article>
							<?php endwhile; ?>
							<?php   // default labels
    // $next_label = 'Next Newsletter';
    // $prev_label =  'Previous Newsletter';
    ?>

    <div class="page-navigation clearfix news-related">
    	<div class="nav-previous" style="display: block;">
        <?php
     // Set max_num_pages for next_posts_link() when using a custom query (see the Codex).
     // Get the max_num_pages from the custom query object ($the_query)
     next_posts_link( '← Previous Newsletter', $the_query->max_num_pages );     ?>

    	</div>
    	<div class="nav-next" style="display: block;">
     <?php previous_posts_link( 'Next Newsletter →' ); ?>
    	</div>
    </div>
		<?php wp_reset_query(); ?>




					<?php else: ?>

					<div id="post-404" class="noposts">

						<p><?php _e('None found.','example'); ?></p>

					</div><!-- /#post-404 -->

				<?php endif;?>

		</div>
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <aside id="sidebar">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
      </aside><!-- .widget-area -->
    <?php endif; ?>

	</div>
</span>
</div><!-- /#content -->

<?php get_footer(); ?>
