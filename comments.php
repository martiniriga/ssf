<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage ssf
 * @since ssf 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */


 if (in_category('newsletter_so')||in_category('blog_so'))
{
	$email ='Email-ka';
	$comment ='Fariin : Wax noo soo qor';
	$title= 'Jawaab ina soo sii';
}
else {
		$email ='Email : mail@example.com';
		$comment ='Message : Write something to us';
		$title= 'Give us your Feedback';
	}
	
?>
<div id="comments" class="comments-area">

	 <?php
  $comment_args = array( 'title_reply'=> $title,

	'fields' => apply_filters( 'comment_form_default_fields', array(

	'author' => '<p class="email">' .
                 '* <input id="author" name="author" type="text" placeholder="Name : John Doe" value="" aria-required="true" />'.
                  '</p>',
    'url'    => '',

    'email'  => '<p class="email">' .
                 '* <input id="email" name="email" type="email" placeholder="'.$email.'" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" aria-required="true" />'.
                  '</p>',

     ) ),

    'comment_field' => '<p class="text">' .
                '* <textarea id="comment" name="comment" placeholder="'.$comment.'" aria-required="true"></textarea>' .
                '</p>',

    'comment_notes_after' => '',

);

comment_form($comment_args);
// comment_form();


?>

</div>
