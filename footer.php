<div class="donor-wrapper">
	<a target="_blank" href="http://somalia.um.dk/"><img src="<?php bloginfo('template_url'); ?>/images/danida.jpg" class="donor-image" /></a>
	<a target="_blank" href="http://eeas.europa.eu/delegations/somalia/index_en.htm"><img src="<?php bloginfo('template_url'); ?>/images/eu.jpg" class="donor-image" /></a>
	<a target="_blank" href="http://kenia.nlembassy.org/organization/departments/somalia.html"><img src="<?php bloginfo('template_url'); ?>/images/nether.jpg" class="donor-image" /></a>
	<a target="_blank" href="https://www.regjeringen.no/en/dep/ud/id833/"><img src="<?php bloginfo('template_url'); ?>/images/norway.png" class="donor-image" /></a>
	<a target="_blank" href="http://www.swemfa.se/region/africa/somalia/"><img src="<?php bloginfo('template_url'); ?>/images/sweden.JPG" class="donor-image" /></a>
	<a target="_blank" href="https://www.gov.uk/government/world/somalia"><img src="<?php bloginfo('template_url'); ?>/images/uk.jpg" class="donor-image" /></a>
</div>
</div><!--content-section -->
<div class="bottom-section">
	<br />
	<span class="copyright">
		© Copyright. Somalia Stability Fund, 2014 |


		<a href="/ssf/legal-policy" class="legal-policy">
			Legal Policy
		</a>
	</span>
	<span class="" style="font-size: 12px;margin-left: 21%;">
		<a target="_blank" href="https://somaliastabilityke.sharepoint.com/sites/inv_dashboard/">Investee Login</a>
	</span>
	<span class="designer">
		<a href="http://www.masterpiecenet.co.ke">
			Masterpiece Fusion
		</a>
	</span>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
