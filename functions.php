<?php

if ( ! function_exists( 'ssf_setup' ) ) :

	function ssf_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'html5', array(
   	'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
   	) );
    add_theme_support( 'post-formats', array(
    	'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    	) );
		add_theme_support( 'post-thumbnails' );
    register_nav_menus( array( 'primary' => __( 'Primary Menu',  'main' )) );
}

add_action( 'after_setup_theme', 'ssf_setup' );

endif;

if ( ! function_exists( 'ssf_fonts_url' ) ) :

function ssf_fonts_url()
{
    $fonts_url = '';
    $fonts     = array();
    $subsets   = 'latin,latin-ext';
    if ( 'off' !== _x( 'on', 'Lato font: on or off', 'ssf' ) ) {
        $fonts[] = 'Lato:400,700';
    }
    if ( $fonts ) {
        $fonts_url = add_query_arg( array(
            'family' => urlencode( implode( '|', $fonts ) ),
            'subset' => urlencode( $subsets ),
        ), 'https://fonts.googleapis.com/css' );
    }

    return $fonts_url;
}
endif;


if ( ! function_exists( 'ssf_enqueue_scripts' ) ) :

  function ssf_enqueue_scripts()
 {
    wp_enqueue_style( 'ssf-fonts', ssf_fonts_url(), array(), null );

	wp_deregister_style( 'main' );
	wp_enqueue_style( 'main',  get_template_directory_uri() . '/css/main.css', false, null, 'all');

	wp_deregister_style( 'toast' );
	wp_enqueue_style( 'toast', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css', false, null);


    if (is_page('background') || is_page('background_so') || is_page('apply') || is_page('apply_so')) {
        wp_deregister_style( 'timelines' );
        wp_enqueue_style( 'timelines',  get_template_directory_uri() . '/css/timelines.css', false, null, 'all');
    }
	if (!is_page('index_so')|| !is_front()) {
        wp_deregister_style( 'layouts' );
        wp_enqueue_style( 'layouts',  get_template_directory_uri() . '/css/layouts.css', false, null, 'all');
    }
	wp_deregister_style( 'fontawesome' );
        wp_enqueue_style( 'fontawesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false, null, 'all');


     if ( is_home() || is_single() || is_category() ||is_archive() ){
         wp_deregister_style( 'newsletter' );
         wp_enqueue_style( 'newsletter',  get_template_directory_uri() . '/css/newsletter.css', false, null, 'all');

        if (in_category('blog') || in_category('blog_so'))
        {
            wp_deregister_style( 'blog' );
            wp_enqueue_style( 'blog',  get_template_directory_uri() . '/css/blog.css', false, null, 'all');
        }
        else{
            wp_deregister_style( 'newsletter' );
            wp_enqueue_style( 'newsletter',  get_template_directory_uri() . '/css/newsletter.css', false, null, 'all');
            wp_deregister_style( 'newsletter' );
            wp_enqueue_style( 'newsletter',  get_template_directory_uri() . '/css/newsletter.css', false, null, 'all');
        }

     }
        wp_deregister_style( 'responsive' );
	wp_enqueue_style( 'responsive',  get_template_directory_uri() . '/css/responsive.css', false, null, 'all');


    wp_deregister_script( 'jquery' );
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js", array(),false, false);

		if ( is_page('contact') || is_page('application-form')) {
			wp_enqueue_script('jquery');
			wp_enqueue_script( 'jquery-form' );
		}

	wp_deregister_script( 'cookie' );
        wp_enqueue_script( 'cookie','https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js', array('jquery'), false, true);

        wp_deregister_script( 'slicknav' );
	wp_enqueue_script( 'slicknav', 'https://cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.5.5/jquery.slicknav.min.js', false, null, true);

	wp_deregister_script( 'newmenu' );
	wp_enqueue_script( 'newmenu', get_template_directory_uri() . '/js/newmenu.js', false, null, true);

	wp_deregister_script( 'toast' );
	wp_enqueue_script( 'toast', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js', false, null,true);

	if ( is_page('structure') || is_page('structure_so') || is_page('our-approach_so') || is_page('approach')){
 	wp_deregister_script( 'hover' );
 	wp_enqueue_script( 'hover', get_template_directory_uri() . '/js/hover.js', false, null, true);
  }



	if ( is_page('faq')|| is_page('faq_so')){
		wp_deregister_script( 'jqueryui' );
                wp_enqueue_script( 'jqueryui','https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', array('jquery'));
		wp_deregister_script( 'accordion' );
		wp_enqueue_script( 'accordion', get_template_directory_uri() . '/js/accordion.js', false, null,true);

	}
	if (is_page('background') || is_page('background_so') || is_page('apply') || is_page('apply_so')) {
	    wp_deregister_script( 'timelines' );
	    wp_enqueue_script( 'timelines',  get_template_directory_uri() . '/js/timelines.js', false, null, 'all');
	}
 	if(is_page('investments')){
	  wp_deregister_script( 'map' );
	  wp_enqueue_script( 'map',  'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', false, null, true);
          wp_deregister_script( 'papaparse' );
	  wp_enqueue_script( 'papaparse',  'https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.1.2/papaparse.min.js', false, null, true);
          wp_deregister_script( 'alasql' );
	  wp_enqueue_script( 'alasql',  'https://cdnjs.cloudflare.com/ajax/libs/alasql/0.2.5/alasql.min.js', false, null, true);
	  wp_deregister_script( 'mp' );
	  wp_enqueue_script( 'mp',  get_template_directory_uri() . '/js/mp.js', false, null, true);
        //create siteurl accessible in js as WPURLS.siteurl
          $myurl= array( 'template_url' => get_bloginfo('template_url') );
          wp_localize_script( 'mp', 'myurl', $myurl);
	}

}
add_action( 'wp_enqueue_scripts', 'ssf_enqueue_scripts' );

endif;



function ssf_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page()  ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) );
	if(is_page('so_index'))
		$title = "Somali Stability Fund";
	return $title;
}
add_filter( 'wp_title', 'ssf_wp_title', 10, 2 );

function ssf_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Newsletter Widget', 'ssf' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your Newsletter sidebar.', 'ssf' ),
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
	register_sidebar( array(
		'name'          => __( 'Somali Newsletter Widget Area', 'ssf' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your Somali Newsletter sidebar.', 'ssf' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
		) );
  register_sidebar( array(
        'name'          => __( 'Blog Widget', 'ssf' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Add widgets here to appear in your Blog sidebar.', 'ssf' ),
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
  register_sidebar( array(
        'name'          => __( 'Somali Blog Widget', 'ssf' ),
        'id'            => 'sidebar-4',
        'description'   => __( 'Add widgets here to appear in your Somali Blog sidebar.', 'ssf' ),
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
}
add_action( 'widgets_init', 'ssf_widgets_init' );

add_action( 'pre_get_posts', 'get_month_posts' );
function get_month_posts( &$wp_query )
{
	if(is_category('newsletter')||is_category('newsletter_so')){
		$wp_query->set('date_pagination_type', 'monthly');
	}
}

if(isset($_COOKIE['language']) && $_COOKIE['language']=='somalia')
add_filter('get_archives_link', 'translate_archive_month');
function translate_archive_month($list) {

  $patterns = array(
    '/January/', '/February/', '/March/', '/April/', '/May/', '/June/',
    '/July/', '/August/', '/September/', '/October/',  '/November/', '/December/'
  );

  $replacements = array( //PUT HERE WHATEVER YOU NEED
    'Jannaayo', 'Febraayo', 'Maarso', 'Abriil', 'Maajo', 'Juun',
    'Julay', 'Agoosto', 'Sebtembar', 'Oktoobar', 'Nofembar', 'Desembar'
  );

  $list = preg_replace($patterns, $replacements, $list);
return $list;
}

//Ug%5M*Zzn(EcA4R3xH
//delete post revision data
// DELETE a,b,c
// FROM `wp_posts` a
// LEFT JOIN `wp_term_relationships` b ON (a.ID = b.object_id)
// LEFT JOIN `wp_postmeta` c ON (a.ID = c.post_id)
// WHERE a.post_type = 'revision'
//
function upload_user_file( $file = array() ) {
	require_once( ABSPATH . 'wp-admin/includes/admin.php' );
      $file_return = wp_handle_upload( $file, array('test_form' => false ) );
      if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
          return false;
      } else {
          $filename = $file_return['file'];
          $attachment = array(
              'post_mime_type' => $file_return['type'],
              'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
              'post_content' => '',
              'post_status' => 'inherit',
              'guid' => $file_return['url']
          );
          $attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
          require_once(ABSPATH . 'wp-admin/includes/image.php');
          $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
          wp_update_attachment_metadata( $attachment_id, $attachment_data );
          if( 0 < intval( $attachment_id ) ) {
          	return $attachment_id;
          }
      }
      return false;
}

function appform_add_script() {
	if (is_page('application-form')) {
		wp_enqueue_script( 'appform_js', get_template_directory_uri().'/js/application.js', array('toast') );
		wp_localize_script( 'appform_js', 'ajax_object',array('ajax_url' => admin_url( 'admin-ajax.php' ),));
	}
}
add_action('wp_enqueue_scripts', 'appform_add_script');

function appform_markup() {
  if (is_page('application-form')) {

if($_COOKIE['language']=='somalia'){
	$pagetitle='GUDBI';
	$p='Fadlan halkaan soo geli oo ku soo gudbi fikradaha  mashaariicda.​​​​​';
	$name='Magac';
	$email='Ciwaanka Email-ka';
	$investment='Lambarka Aqoonsiga Maalgelinta';
	$button='GUDBI';
}else{
	$pagetitle='Submit Application';
	$p='Please upload and submit your concept note.​​​​​​​';
	$name='Your Name';
	$email='Your Email-address';
	$investment='Your investment id';
	$button='Submit';
}

$markup = <<<EOT

<div class="pagetitle">{$pagetitle}</div>
<div class="page-line-hr"></div>
<div class="left-side left-side-four left-side-one">
<div class="left-side-content">
<div class="ms-rtestate-field" style="display:inline"><p></p>​{$p}</div>
</div>
</div>
<div class="right-side right-side-four">
  <form id="app">
                <p class="name">
                    <input name="name" type="text" class="feedback-input" placeholder="{$name}" id="name">
                </p>
                <p class="email">
                    <input name="email" type="text" class="feedback-input" id="email" placeholder="{$email}">
                </p>
                <p class="investment">
                    <input name="investment" type="text" class="feedback-input" id="investment" placeholder="{$investment}">
                </p>
                <h2 class="file-header">Files Upload</h2>
                <p class="files">
                    <span class="file-label">File [1]</span>
                    <input name="file1" type="file" class="files-upload" id="file1">
                </p>
                <p class="files">
                    <span class="file-label">File [2]</span>
                    <input name="file2" type="file" class="files-upload" id="file2">
                </p>
                <p class="files">
                    <span class="file-label">File [3]</span>
                    <input name="file3" type="file" class="files-upload" id="file3">
                </p>
                <p class="files">
                    <span class="file-label">File [4]</span>
                    <input name="file4" type="file" class="files-upload" id="file4">
                </p>
                <div class="submit">
                    <button type="submit" id="button-blue" name="">{$button}</button>
                    <div class="ease">
                    </div>
                </div>
		<input type="hidden" name="application_form_submitted" value="appform_action">

            </div>
</form>

EOT;

return $markup;
}
}
add_shortcode('application_form', 'appform_markup');

function ajax_app() {
	if (isset($_POST['application_form_submitted']))
	{
		 $name  = ( isset($_POST['name']) )  ? trim(strip_tags($_POST['name'])) : null;
		 $email   = ( isset($_POST['email']) )   ? trim(strip_tags($_POST['email'])) : null;
		 $investment = ( isset($_POST['investment']) ) ? trim(strip_tags($_POST['investment'])) : null;
		 $file1=" "; $file2=" "; $file3=" "; $file4=" ";
		 if ( ! function_exists( 'wp_handle_upload' ) ) {
			 	require_once( ABSPATH . 'wp-admin/includes/admin.php' );
		    require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		require_once( ABSPATH . 'wp-content/themes/ssf/functions.php' );
		if(isset($_FILES['file1']))
		 $file1 = upload_user_file($_FILES['file1']);
		if(isset($_FILES['file2']))
		 $file2 = upload_user_file($_FILES['file2']);
		if(isset($_FILES['file3']))
		 $file3 = upload_user_file($_FILES['file3']);
		if(isset($_FILES['file4']))
		 $file4 = upload_user_file($_FILES['file4']);
		 $error ="";
		 if ( $name == '' )
		 	$error .= 'Error: please fill the required field (name).';
		 if ( !is_email($email) )
		 	$error .= 'Error: please enter a valid email address.';
		 if ( $investment == '' )
		 $error .= 'Error: please fill the required field (investment_id).';
		 $mymsg = array(
		             1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
		             2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
		             3=>"The uploaded file was only partially uploaded",
		             4=>"No file was uploaded",
		             6=>"Missing a temporary folder" );

		if ( $file1 == false && $_FILES['file1']['error'] >0) {
			$error .= "file 1:".$mymsg[$file_error]." <br>";
		}
		if ( $file2 == false && $_FILES['file1']['error'] >0 ) {
			$error .= "file 2:".$mymsg[$file_error]." <br>";
		}
		if ( $file3 == false && $_FILES['file1']['error'] >0 ) {
			$error .= "file 3:".$mymsg[$file_error]." <br>";
		}
		if ( $file4 == false && $_FILES['file1']['error'] >0 ) {
			$error .= "file 4:".$mymsg[$file_error]." <br>";
		}
		if ( $error !="" ) {
			wp_send_json(array('type'=>'error', 'text' => $error ));
		}
		require_once( ABSPATH . 'wp-content/themes/ssf/functions.php' );
		global $wpdb;
		$wpdb->query("INSERT INTO wp_submissions (name, email, investment_id,file1,file2,file3,file4) VALUES ('$name', '$email', '$investment','$file1','$file2','$file3','$file4')"  );
		wp_send_json(array('type'=>'success','text' => 'successfully submited your concept notes'));
 	  exit();
	}

}
add_action( 'wp_ajax_ajaxapp', 'ajax_app' );
add_action( 'wp_ajax_nopriv_ajaxapp', 'ajax_app' );

function handle_contact() {
	if(!isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'cf' ) ) wp_send_json_error('Invalid request');
	$params = array();
	parse_str($_REQUEST['serialized'],$params);
	if(isset( $params['name'] ))
	$name = filter_var($params['name'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	if(isset( $params['email'] ))
	$email = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
	if(isset( $params['subject'] ))
	$subject = filter_var($params['subject'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	if(isset( $params['message'] ))
	$message = stripslashes($params['message']);
	$message .= PHP_EOL.PHP_EOL.'IP address: '.$_SERVER['REMOTE_ADDR'];
	$message .= PHP_EOL.'Sender\'s name: '.$name;
	$message .= PHP_EOL.'E-mail address: '.$email;
	$to = array('info@stabilityfund.so', get_option('admin_email') ); // If you like change this email address
						// replace "noreply@yourdomain.com" with your real email address
	$header = 'From: '.get_option('blogname').' <noreply@stabilityfund.so>'.PHP_EOL;
	$header .= 'Reply-To: '.$email.PHP_EOL;
	if ( wp_mail($to, $subject, $message, $header) )
		{
			wp_send_json_success('Your Form has been submitted, We Will contact you as soon as possible');
		}
	else
		{
			wp_send_json_error();
		}

}

function contact_js() {
	if (is_page('contact')) {
		wp_enqueue_script( 'cf_js', get_template_directory_uri().'/js/contact.js', array('jquery','toast') );
		wp_localize_script( 'cf_js', 'cf', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				 'nonce'       => wp_create_nonce( 'cf' ),
		) );
	}
}

function contact_markup() {

if($_COOKIE['language']=='somalia')
{
	$pagetitle='NALA SOO XIRIIR';
	$p='<div>​​Wax su’aal ah ma qabtaa?</div><div>Ama ma dooneysaa in aad wax dheeri ah ka ogaato?</div><div>Fadlan nala soo xiriir. ​​​</div>​​​​​';
	$name='Magac';
	$email='Ciwaanka Email-ka';
	$subject='Mowduuc';
	$message='Farriin';
	$button='GUDBI';
}
else
{
	$pagetitle='CONTACT US';
	$p='<div>​​Do you have a question?</div><div>Or want to learn more?</div><div>Please get in touch.​​</div>​​​​​​​';
	$name='Your name';
	$email='Your email-address';
	$subject='subject';
	$message="message";
	$button='Send us a Message';
}
$markup = <<<EOT

<div class="pagetitle">{$pagetitle}</div>
<div class="page-line-hr"></div>
<div class="left-side left-side-four left-side-one">
<div class="left-side-content">
		<div>{$p}</div>
</div>
</div>
<div class="right-side right-side-four">
  <form action="" id="app" method="post">
      <p class="name">
        <input name="name" type="text" class="feedback-input" placeholder="{$name}" id="name">
      </p>
      <p class="email">
        <input name="email" type="text" class="feedback-input" id="email" placeholder="{$email}">
      </p>
			<p class="subject">
        <input name="subject" type="text" class="feedback-input" id="subject" placeholder="{$subject}">
      </p>
			<p class="text">
        <textarea name="message" class="feedback-input" id="message" placeholder="{$message}"></textarea>
      </p>
      <div class="submit">
        <button type="submit" id="button-blue" name="btnsubmit">{$button}</button>
      </div>
	</form>
</div>

EOT;

if (is_page('contact')) return $markup;

}

add_action( 'wp_ajax_cf', 'handle_contact' );
add_action( 'wp_ajax_nopriv_cf', 'handle_contact' );
add_action('wp_enqueue_scripts', 'contact_js');
add_shortcode('contact_form', 'contact_markup');

add_action( 'phpmailer_init', 'configure_smtp' );
function configure_smtp( PHPMailer $phpmailer ){
    $phpmailer->isSMTP(); //switch to smtp
    $phpmailer->Host = 'n1plcpnl0078.prod.ams1.secureserver.net';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 465;
    $phpmailer->Username = 'somstabilityfund@stabilityfund.so';
    $phpmailer->Password = 'M0qud1shu';
    // $phpmailer->SMTPSecure = true;
		$phpmailer->SMTPSecure = "tls";
    $phpmailer->From = 'somstabilityfund@stabilityfund.so';
    $phpmailer->FromName='Somalia Stability Fund';
		if( !$phpmailer->send() ) {
        echo "Mailer Error: " . $phpmailer->ErrorInfo;
    } else {
        echo "Message sent!";
    }
}
 ?>
