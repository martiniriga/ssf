<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=10">
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta http-equiv="Expires" content="0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">

	<link rel="icon" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon.png">
	<?php wp_head(); ?>
</head>
<body>
	<div class="container">
		<div class="top-section">
			<div class="language">
				<span class="languages">
					<a href="/" id="english" class="english active">English</a>
				</span>
				<span class="languages">
				<a href="/so_index" id="somalia" class="somalia">Somali</a>
				</span>
			</div>
			<!-- Subscribe -->
			<div id="subscribe">
				<ul id="ssf-logins">
					<li>
						Login
						<ul>
							<li><a href="http://somaliastabilityke.sharepoint.com/sites/inv_dashboard" target="_blank">Investee Dashboard</a></li>
							<li><a href="http://somaliastabilityke.sharepoint.com/home" target="_blank">SSF Intranet</a></li>
							<li><a href="http://somaliastabilityke.sharepoint.com/sites/stats" target="_blank">Donor Portfolio</a></li>
						</ul>
					</li>
				</ul>

				<span style="font-weight:bold;font-size:1.2em">
					SUBSCRIBE :
				</span>
				<input name="subscribe" id="subscribebox" type="email" size="25" required="required" placeholder="Enter Email...">
				<button class="button-blue-one-one">
					Go
				</button>

			</div>
			<!-- End Subscribe-->
			<div class="social">
				<a href="https://twitter.com/stability_fund" target="_blank" class="social-tweet">
					<i class="fa fa-twitter"></i>
				</a>
				<a href="https://www.facebook.com/stabiltyfund.so?fref=ts" target="_blank" class="social-fb">
					<i class="fa fa-facebook"></i>
				</a>
				<a href="https://instagram.com/stability_fund/" target="_blank" class="social-inst">
					<i class="fa fa-instagram"></i>
				</a>
				<a href="http://www.linkedin.com/company/somalia-stability-fund" target="_blank" class="social-in">
					<i class="fa fa-linkedin"></i>
				</a>
			</div>
		</div>
		<!-- Logo -->
		<div data-name="SiteLogo" class="ssf-logo">
			<div>
				<a title="Website" class="logo-main" href="/"><img class="logo-image" name="onetidHeadbnnr0" src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Website" /></a>
			</div>
		</div>
		<?php
                if( isset($_COOKIE['language']) && $_COOKIE['language'] == 'somalia' && ! is_admin()) {
                   $menu = 'Somali Menu';
                   $menu_class='somalia-menu';
                 } else {
                   $menu = 'English Menu';
                   $menu_class='english-menu';
                 } 
		$defaults = array(
                        'menu' => $menu,
			'container'       => 'div',
			'container_id'    => 'ssfmenu',
			'menu_class'      => $menu_class,
			'menu_id'         => 'topnav',
			'depth'           => 2,
		);
		wp_nav_menu($defaults); ?>
		

		<div class="content-section">