var $timeline_block;
function timelines_loaded(){
	$('.left-side').height(function(){return $('#cd-timeline').height() + 26}).css('margin-bottom','0px');
	$timeline_block = $('.cd-timeline-block');

	//on scolling, show/animate timeline blocks when enter the viewport
	$(".content-section").scroll(function(){
		$timeline_block.each(function(){
			if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				//$(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
				$(this).prev().find('.cd-timeline-img').removeClass('cd-active').addClass('cd-inactive');
				$(this).find('.cd-timeline-img').removeClass('cd-inactive').addClass('cd-active');
				$(this).find('.cd-date').addClass('cd-ssf-color');
			}
			else if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 ){
				$(this).next().find('.cd-timeline-img').removeClass('cd-active').addClass('cd-inactive');
				$(this).prev().find('.cd-timeline-img').removeClass('cd-active').addClass('cd-inactive');
				$(this).find('.cd-timeline-img').removeClass('cd-inactive').addClass('cd-active');
				$(this).prev().find('.cd-date').removeClass('cd-ssf-color');
				$(this).next().find('.cd-date').removeClass('cd-ssf-color');
				$(this).find('.cd-date').addClass('cd-ssf-color');
			}
		});
	});

}
