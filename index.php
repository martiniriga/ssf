<?php get_header(); ?>
<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/blog.css"> -->
<div id="content">
<span id="DeltaPlaceHolderMain">
	<?php //query_posts('post_type=post&post_status=publish&posts_per_page=10&paged='. get_query_var('paged')); ?>
	<table cellpadding="0" cellspacing="0" border="0" width="95%">
		<tbody><tr>
			<td>
				<table cellpadding="0" cellspacing="0" class="ms-blog-MainArea">
					<tbody><tr>
						<td valign="top">
							<?php if( have_posts() ): ?>
							<ul class="ms-blog-postList">

								<?php while( have_posts() ): the_post(); ?>
								<li>
									<div class="ms-blog-postBox ms-shadow">
										<div class="ms-blog-postBoxDate">
											<div class="ms-textSmall"><?php the_time('Y'); ?></div>
											<div class="ms-textXLarge ms-blog-dateText"><?php the_time('n/j'); ?></div>
										</div>
										<div class="ms-blog-postBoxMargin">
											<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
											<div class="ms-metadata ms-textSmall">
												<span>by <span class="ms-noWrap ms-imnSpan"><?php the_author_link(); ?></span> at <?php the_time('F jS, Y'); ?> in <a href="https://somaliastabilityke-public.sharepoint.com/Blog/Category/6/Blog" id="blgcat" class="ms-link">Blog</a></span>
											</div>
											<p></p>
											<div class="ms-blog-postBody">
												<?php the_content(); ?>
												<!-- <a target="_blank" class="special-read-more" href="/newsletter?ID=33" data-som="/Pages/so/newsletter.aspx?ID=34" style="color:#3693cc;text-decoration:none;word-break:break-word;">Read more</a> -->
											</div>
											<p></p>
											<div>
												<span id="commandBar1">
													<span class="ms-noWrap">
														<ul class="ms-comm-forumCmdList">
															<li id="commandBar1-share_WPQ1_15-Root" class="ms-comm-cmdSpaceListItem">
																<a title="Email a link" href="#" id="commandBar1-share_WPQ1_15-Link" class="ms-secondaryCommandLink">Email a link
																</a>
															</li>
														</ul>
													</span>
												</span>
											</div>
										</div>
									</div>
									<div class="ms-blog-postDivider"></div>
								</li>
							<?php endwhile; ?>

							<div class="navigation">
								<span class="newer"><?php previous_posts_link(__('« Newer','example')) ?></span> <span class="older"><?php next_posts_link(__('Older »','example')) ?></span>
							</div><!-- /.navigation -->
						</ul>
					<?php else: ?>

					<div id="post-404" class="noposts">

						<p><?php _e('None found.','example'); ?></p>

					</div><!-- /#post-404 -->

				<?php endif; wp_reset_query(); ?>
			</td>
			<td valign="top"class="ms-blog-LeftColumn">
				<div class="ms-webpart-zone ms-fullWidth"></div>
			</td>
		</tr>
	</table>
</td>
</tr>
</table>
</span>
</div><!-- /#content -->

<?php get_footer(); ?>
