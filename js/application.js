jQuery(document).ready(function($) {
  $('#button-blue').click(function(e){
      e.preventDefault();
      var errors = '';

        var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      $('.feedback-input').each(function(){
        if($(this).val()=='')
        errors += $(this).attr('name')+' cannot be blank. <br>';
      });
      if (!email_reg.test($.trim($('#email').val())))
        errors += 'Error: please enter a valid email address. \n';

      if(errors==''){
        var m_data = new FormData();
        if($('#file1').val()!='')
        m_data.append( 'file1', $('input[name=file1]')[0].files[0]);
        if($('#file2').val()!='')
        m_data.append( 'file2', $('input[name=file2]')[0].files[0]);
        if($('#file3').val()!='')
        m_data.append( 'file3', $('input[name=file3]')[0].files[0]);
        if($('#file4').val()!='')
        m_data.append( 'file4', $('input[name=file4]')[0].files[0]);
        var input_data = $('#app').serializeArray();
        $.each(input_data,function(key,input){
          m_data.append(input.name,input.value);
        })
          m_data.append('action','ajaxapp');
          m_data.append('nonce','af.nonce');
        $.ajax({
          url: ajax_object.ajax_url,
          data: m_data,
          processData: false,
          contentType: false,
          type: 'POST',
          datatype:'json',
          success: function (d) {
                toastr[d.type](d.text);
               },
          error: function (xhr, textStatus, error) {
               toastr['error'](error);
           }
        });
      }else{
        toastr['warning']('The following errors were encountered \n'+errors);
      }
  });

});
