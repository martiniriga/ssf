

$.extend($.expr[':'], {
  'containsi': function(elem, i, match, array)
  {
    return (elem.textContent || elem.innerText || '').toLowerCase()
    .indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});

Array.prototype.frequencies  = function () {
    var freqs = {sum: 0};
    this.map( function (a){
        if (!(a in this)) { this[a] = 1; }
        else { this[a] += 1; }
        this.sum += 1;
        return a; }, freqs
    );
    return freqs;
}

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};


function resizeResults(){
		var height = 0;
		height = $("#example-tabs section.current > div:first").height() + 200;
		$('#example-tabs .content').css('height',height+'px');
}


function isIE(){

	if(navigator.userAgent.toLowerCase().indexOf('msie') != -1){
		return true;
	}
	else
    if (/MSIE 10/i.test(navigator.userAgent)) {
	   // this is internet explorer 10
	   return true;
	}
	else
	if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)){
	    // this is internet explorer 9 and 11
	    return true;
	}
	else
	if (/Edge\/12./i.test(navigator.userAgent)){
	   // this is Microsoft Edge
	   return true;
	}
	else
		return false;
}




function formatNum(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}





function notify(heading,message){
	var notification = new NotificationFx({
		message : '<h4 style="text-transform: uppercase;font-weight: bold;font-family: "Alegreya Sans,Segoe Ui";letter-spacing: 0.4px;font-size: 16px">'+heading+'</h4><p style="font-family: "Alegreya Sans,Segoe Ui";font-size: 15px">' + message + '</p>',
		layout : 'attached',
		effect : 'bouncyflip',
		type : 'notice', // notice, warning or error
		ttl : 7000
	});

	// show the notification
	notification.show();
}


function makeData(arr,index){
    return $.map(arr,function(val,i){return val[''+index]})
}




$('.content-wrapper').on('click','.loadMap',function(){
	setLoader();
    clearContent();
    $('#donor-wrapper .donor-nav-item').removeClass('menuactive');
    $(this).addClass('menuactive');
    $('#donor-wrapper .main-content').append("<div id='map'></div><div id='maplegend'><h3>Legend</h3></div><div id='maplegend1'><h3>IM Score</h3></div>");
    mapInvestments(locations,investments);
    removeLoader();
    resizeSidebar();
});





var current_list = '';
var listName = '';
var results,results_count,results_item_count;

$('.content-wrapper').on('click','.loadDocs',function(){
	 setLoader();
     clearContent();
     $('#donor-wrapper .donor-nav-item').removeClass('menuactive');
     $(this).addClass('menuactive');
     $('#donor-wrapper .main-content').load('/sites/stats/Site%20Assets/layouts/docs.html',function(){
		$('.doc-folders').off().on('click', function(){
			listName = $(this).parent().attr('id');
			current_list = $(this).parent().attr('title');
			$('.document-wrapper').removeClass('lists-home');
			$('.breadcrumbs').append('<a href="javascript:void(0)" class="navigator list-level" title="'+ current_list +'" id="'+ listName +'">'+ listName +'</a>');
			getDocuments();
		});

		var lists_inv = $('.doc-folder-title').parent().parent().parent().map(function(){
						return {id : $(this).attr('id'), list : $(this).attr('title')}
					   });
       removeLoader();
       $('#donor-wrapper .sidebar').css('height','458px');
     });
});


$('.content-wrapper').on('click','.loadResults',function(){
	 setLoader();
     clearContent();
     $('#donor-wrapper .donor-nav-item').removeClass('menuactive');
     $(this).addClass('menuactive');
     $('#donor-wrapper .main-content').load('/sites/stats/Site%20Assets/layouts/results_reporting.html',function(){
		$("#example-tabs").steps({
		        headerTag: "h3",
		        bodyTag: "section",
		        transitionEffect: "slideLeft",
		        enableFinishButton: false,
		        enablePagination: false,
		        enableAllSteps: true,
		        titleTemplate: "#title#",
		        cssClass: "tabcontrol"
		});

		$('.previous-section').click(function(){
		    $('.steps ul li.current').prev().find('a').click();
		    resizeResults();
		    resizeSidebar();
		    $('.next-section').show();
		    if($("#example-tabs .content section:first").is($("#example-tabs .content section.current"))){
		        $('.previous-section').hide();
		    }
		    $('section.current > div:first').scroller('destroy');
			$('section.current > div:first').scroller({horizontal: true});
		});
		$('.next-section').click(function(){
		    $('.steps ul li.current').next().find('a').click();
		    resizeResults();
		    resizeSidebar();
		    $('.previous-section').show();
		    if($("#example-tabs .content section:last").is($("#example-tabs .content section.current"))){
		        $('.next-section').hide();
		    }
		    $('section.current > div:first').scroller('destroy');
			$('section.current > div:first').scroller({horizontal: true});
		});


		$('.steps ul li a').click(function(){
			resizeResults();
			resizeSidebar();
		});

		$('.steps ul li').click(function(){
		    if($(this).hasClass('first')){
		        $('.previous-section').hide();
		        $('.next-section').show();
		    }
		    else if($(this).hasClass('last')){
		        $('.next-section').hide();
		        $('.previous-section').show();
		    }
		    else{
		        $('.next-section').show();
		        $('.previous-section').show();
		    }
		    $('section.current > div:first').scroller('destroy');
			$('section.current > div:first').scroller({horizontal: true});
		});




	   resizeResults();
       removeLoader();
       resizeSidebar();
     });
});


$('.content-wrapper').on('click','.loadMatrix',function(){
	 setLoader();
     clearContent();
     $('#donor-wrapper .donor-nav-item').removeClass('menuactive');
     $(this).addClass('menuactive');
     $('#donor-wrapper .main-content').load('/sites/stats/Site%20Assets/layouts/inv_matrix.html',function(){
     	loadMatrix();
	   //getInvMatrixData();
	   //getInvMatrixDocs();
       removeLoader();
       resizeSidebar()
     });
});








function setLoader(){
	$('.loaderwrapper').fadeIn();
}

function removeLoader(){
	$('.loaderwrapper').fadeOut();
}


function clearContent(){
    $('#donor-wrapper .main-content').empty();
}

function filterMapStatus(status){
	var query = '';
	if(status == 'all'){
		query = '';
	}
	else{
		if(status.indexOf('/') == '-1'){
			query = 'WHERE status = "' + status + '"';
		}
		else{
			var sep = status.split('/');
			query = 'WHERE status = "' + sep[0] + '" OR status = "' + sep[1] + '"';
		}
	}
	var sql =  'SELECT * from ? ' + query;
	var inv = alasql(sql,[investments_data]);
	var locations = makeData(inv,'location');
	clearContent();
    $('#donor-wrapper .main-content').append("<div id='map'></div><div id='maplegend'><h3>Legend</h3></div><div id='maplegend1'><h3>IM Score</h3></div>");
    //console.log(inv);
	mapInvestments(locations,inv);
	//console.log(locations);
}

function getInvImages(){
	var folder = "";
	var listName = "Investment Images";
	$().SPServices({
	    webURL: window.location.origin + '/sites/stats',
	      operation: "GetListItems",
	      async: true,
	      listName: listName,
		  CAMLQueryOptions : "<QueryOptions><Folder>" + listName + "</Folder><ViewAttributes Scope='Recursive'/></QueryOptions>",
	      completefunc: function (xData, Status) {
	        var item_count = $(xData.responseXML).SPFilterNode("rs:data").attr('ItemCount');
	        num = 0;
	        $(xData.responseXML).SPFilterNode("z:row").each(function() {
	        	inv_images.push({
				         		refcode : $(this).attr('ows_Ref_x0020_Code').split(';#')[1],
				         		url : '/' + $(this).attr('ows_FileRef').split(';#')[1],
				         		name : $(this).attr('ows_LinkFilename').substr(0,$(this).attr('ows_LinkFilename').lastIndexOf('.'))
				         	});
	        });

	      }
	  });
}
/*
function getInvImages(){
	var folder = "";
	var listName = "Investment Images";
	$().SPServices({
	    webURL: window.location.origin + '/sites/stats',
	      operation: "GetListItems",
	      async: true,
	      listName: listName,
	      completefunc: function (xData, Status) {
	        var item_count = $(xData.responseXML).SPFilterNode("rs:data").attr('ItemCount');
	        $(xData.responseXML).SPFilterNode("z:row").each(function() {
	         	folder = $(this).attr('ows_LinkFilename');
	         	$().SPServices({
				    webURL: window.location.origin + '/sites/stats',
				      operation: "GetListItems",
				      async: true,
				      listName: "Investment Images",
				      CAMLQueryOptions : "<QueryOptions><Folder>"+STSHtmlEncode(listName) + '/' + folder + "</Folder></QueryOptions>",
				      completefunc: function (xData, Status) {
				        var item_count = $(xData.responseXML).SPFilterNode("rs:data").attr('ItemCount');
				        $(xData.responseXML).SPFilterNode("z:row").each(function() {
				         	inv_images.push({
				         		refcode : folder,
				         		url : '/' + $(this).attr('ows_FileRef').split(';#')[1],
				         		name : $(this).attr('ows_LinkFilename').substr(0,$(this).attr('ows_LinkFilename').lastIndexOf('.'))
				         	});
				        });

				      }
				  });
	        });

	      }
	  });
}
*/


var chart_data = null;
var investments = [];
var infowindows = [];
var markers = [];
var loc = '';
var locations = null;




function mapInvestments(arr, inv_orig){
    var myLatLng = {lat: 5.105, lng: 44.976};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: myLatLng,
        minZoom: 5
    });

    google.maps.event.addListener(map, "click", function(event) {
	    closeAllInfoWindows();
	});
	google.maps.event.addListenerOnce(map, 'idle', function(){
    	$('#map > div > div:nth-child(2),#map > div > div:nth-child(4),#map > div > div.gmnoprint.gm-style-cc').hide();
    	removeLoader();
	});

	google.maps.event.addDomListener(window, "resize", function() {
	   var center = map.getCenter();
	   google.maps.event.trigger(map, "resize");
	   map.setCenter(center);
	});

	var invstatus = $.map(makeData(inv_orig,'status'),function(val,i){return val.toLowerCase().replace(' ','').replace('-','')});

    locs = {
        "Locations": {
	        "Baidoa": {
	            "lat": 3.1140502,
	            "lng": 43.651925000000006
	        },
	        "Balcad": {
	            "lat": 2.3583953,
	            "lng": 45.386288899999954
	        },
	        "Jowhar": {
	            "lat": 2.7774231,
	            "lng": 45.5016253
	        },
	        "Mogadishu": {
	            "lat": 2.0469343,
	            "lng": 45.31816230000004
	        },
	        "Kismayo": {
	            "lat": -0.3560455,
	            "lng": 42.54605730000003
	        },
	        "Beletweyne": {
	            "lat": 4.7429412,
	            "lng": 45.2009362
	        },
	        "Hargeisa": {
	            "lat": 9.562389,
	            "lng": 44.07701339999994
	        },
	        "Garowe": {
	            "lat": 8.4084158,
	            "lng": 48.48372389999997
	        },
	        "Borome": {
	            "lat": 9.936009199999999,
	            "lng": 43.18440199999998
	        },
	        "Burao": {
	            "lat": 9.525916600000002,
	            "lng": 45.53463069999998
	        },
	        "Galkayo": {
	            "lat": 6.7872726,
	            "lng": 47.439235199999985
	        },
	        "Abudwak": {
	            "lat": 6.1481089,
	            "lng": 46.397270400000025
	        },
	        "Bossaso": {
	            "lat": 11.2755407,
	            "lng": 49.18789939999999
	        },
	        "Bari": {
	            "lat": 10.1203847,
	            "lng": 49.6911374
	        },
	        "Galkacyo": {
	            "lat": 6.7872726,
	            "lng": 47.439235199999985
	        },
	        "Adado": {
	            "lat": 11.325,
	            "lng": 48.65777779999996
	        },
	        "Afgooye": {
	            "lat": 2.1426226,
	            "lng": 45.116716699999984
	        },
	        "Badhan": {
	            "lat": 10.7149388,
	            "lng": 48.33647189999999
	        },
	        "Dhahar": {
	            "lat": 9.750082299999999,
	            "lng": 48.82213690000003
	        },
	        "Buuhoodle": {
	            "lat": 8.2363238,
	            "lng": 46.33848160000002
	        },
	        "Laascaanood": {
	            "lat": 8.475987000000002,
	            "lng": 47.365760300000034
	        },
	        "Taleex": {
	            "lat": 9.1490522,
	            "lng": 48.42003269999998
	        },
	        "Middle Jubba": {
	            "lat": 2.0780488,
	            "lng": 41.60118139999997
	        },
	        "Afmadow": {
	            "lat": 0.5150745999999999,
	            "lng": 42.075706099999934
	        },
	        "Garbaharey": {
	            "lat": 3.3304072,
	            "lng": 42.21897809999996
	        },
	        "Qardho": {
	            "lat": 9.511598,
	            "lng": 49.08676439999999
	        },
	        "Hiraan": {
	            "lat": 4.321015,
	            "lng": 45.29938620000007
	        },
	        "Middle Shabelle": {
	            "lat": 2.9250247,
	            "lng": 45.903968899999995
	        },
	        "Badhaadhe": {
	            "lat": -0.793613,
	            "lng": 41.441250999999966
	        },
	        "Dollow": {
	            "lat": 4.164174800000001,
	            "lng": 42.07941630000005
	        },
	        "Belethaawo": {
	            "lat": 3.9239247,
	            "lng": 41.874975500000005
	        },
	        "Luuq": {
	            "lat": 3.8131083,
	            "lng": 42.54605730000003
	        },
	        "Elwak": {
	            "lat": 2.6861757,
	            "lng": 41.441250999999966
	        },
	        "Ishkushuban": {
	            "lat": 10.2822147,
	            "lng": 50.233457199999975
	        },
	        "Towfiiq": {
	            "lat": 2.0740103,
	            "lng": 45.339567699999975
	        },
	        "CeelBuuh": {
	            "lat": 4.683913500000001,
	            "lng": 46.61986079999997
	        },
	        "Dhudhub": {
	            "lat": 9.775555599999999,
	            "lng": 44.87583330000007
	        },
	        "Rako Raxo": {
	            "lat": 9.7836986,
	            "lng": 49.728161099999966
	        },
	        "Xiriiro": {
	            "lat": 9.9770898,
	            "lng": 50.251489799999945
	        },
	        "Godo": {
	            "lat": 3.5039227,
	            "lng": 42.2362435
	        },
	        "bjiraan": {
	            "lat": 4.321015,
	            "lng": 45.29938620000007
	        },
	        "Wajid": {
	            "lat": 3.809294,
	            "lng": 43.2461055
	        },
	        "Dhusamreb": {
	            "lat": 5.5380625,
	            "lng": 46.38646719999997
	        },
	        "Xudur": {
	            "lat": 4.2544385,
	            "lng": 43.94367880000004
	        },
	        "Mahaday": {
	            "lat": 2.9716241,
	            "lng": 45.533451499999956
	        },
	        "Barawe": {
	            "lat": 1.1161954,
	            "lng": 44.03181619999998
	        },
	        "South West": {
	            "lat": 5.152149,
	            "lng": 46.19961599999999
	        },
	        "Lower Juba": {
	            "lat": 0.224021,
	            "lng": 41.60118139999997
	        },
	        "Talex": {
	            "lat": 9.1490522,
	            "lng": 48.42003269999998
	        },
	        "Las Canood": {
	            "lat": 8.475987000000002,
	            "lng": 47.365760300000034
	        },
	        "Erigavo": {
	            "lat": 10.6149654,
	            "lng": 47.365760300000034
	        },
	        "Middle Juba": {
	            "lat": 2.0780488,
	            "lng": 41.60118139999997
	        },
	        "Lower Jubba": {
	            "lat": 0.224021,
	            "lng": 41.60118139999997
	        },
	        "Dhusamareeb": {
	            "lat": 5.5380625,
	            "lng": 46.38646719999997
	        },
	        "Abudwaaq": {
	            "lat": 6.0333868,
	            "lng": 46.36246860000006
	        },
	        "Hobyo": {
	            "lat": 5.3515922,
	            "lng": 48.524975100000006
	        },
	        "Xaradheere": {
	            "lat": 2.3390467,
	            "lng": 42.288005899999916
	        },
	        "Balambale": {
	        	"lat": 8.5,
	        	"lng": 46.28333333333333
		    },
		    "Godobjiraan": {
		        "lat": 7.283333333333333,
		        "lng": 49.36666666666667
		    },
		    "Dangoroyo": {
		        "lat": 8.72686,
		        "lng": 49.3415985
		    },
		    "Warsheikh": {
		        "lat": 2.4203501,
		        "lng": 45.78545
		    },
		    "Harfo": {
		        "lat": 5.8333302,
		        "lng": 48.1833
		    },
		    "Haro": {
		        "lat": 5.8333302,
		        "lng": 48.1833
		    },
		    "Dhoobley": {
		        "lat": 0.4112971,
		        "lng": 41.00889440000003
		    },
			"BeletHawa": {
		        "lat": 3.9275473,
		        "lng": 41.8581997
		    },
		    "Belet Hawa": {
		        "lat": 3.9275473,
		        "lng": 41.8581997
		    },
			"MiddleJubba": {
		        "lat": 2.0780488,
		        "lng": 41.60118139999997
		    },
		    "MiddleJuba": {
		        "lat": 2.0780488,
		        "lng": 41.60118139999997
		    },
			"Jubbaland": {
		        "lat": 3.9318113,
		        "lng": 41.87171680000006
		    },
		    "Jubaland": {
		        "lat": 3.9318113,
		        "lng": 41.87171680000006
		    },
			"Baraawe": {
	            "lat": 1.1161954,
	            "lng": 44.03181619999998
	        },
	        "SouthWest": {
	            "lat": 5.152149,
	            "lng": 46.19961599999999
	        },
			"LowerJubba": {
	            "lat": 0.224021,
	            "lng": 41.60118139999997
	        },
	        "LowerJuba": {
	            "lat": 0.224021,
	            "lng": 41.60118139999997
	        },
	        "Berdale": {
	            "lat": 3.3516667,
	            "lng": 44.12222220000001
	        },
		    "Nationwide":{
	             "lat":4.670835,
	             "lng":46.548332
	          },
	        "Nonspecific":{
	             "lat":9.868190,
	             "lng":49.376860
	          },
	    }
    }

    newarr = arr.slice(0);
    /*for(var i = 0;i<newarr.length;i++){
        newarr[i] = newarr[i].replace('; ','').replace(' & ','').replace(' ','');
    }*/
    var freq = newarr.frequencies();
    var offset = 0.0002;
    markers = [];
    infowindows = [];
    var is_nationwide = "";
	var iconBase = 'https://maps.google.com/mapfiles/ms/micons/';
	//var colors = ['pink-dot','red-dot','orange-dot','ltblue-dot','purple-dot','yellow-dot','green-dot','grey'];
	var mapicons = {
          completed: {
            name: 'Completed/Terminated',
            icon: iconBase + 'red-dot.png'
          },
          terminated: {
            name: 'Completed/Terminated',
            icon: iconBase + 'red-dot.png'
          },
          contracted: {
            name: 'Contracted',
            icon: iconBase + 'green-dot.png'
          },
          /*moupending: {
            name: 'Design/MOU Pending',
            icon: iconBase + 'yellow-dot.png'
          },*/
          committed: {
            name: 'Committed',
            icon: iconBase + 'ltblue-dot.png'
          },
          designphase: {
            name: 'Design phase',
            icon: iconBase + 'purple-dot.png'
          },
          pledged: {
            name: 'Pledged',
            icon: iconBase + 'orange.png'
          }
          /*ddca: {
            name: 'DDCA',
            icon: iconBase + 'purple-dot.png'
          },
          refinement: {
            name: 'Refinement',
            icon: iconBase + 'pink-dot.png'
          },
          tba: {
            name: 'TBA',
            icon: iconBase + 'grey.png'
          },
          ongoing: {
            name: 'Ongoing',
            icon: iconBase + 'orange.png'
          }*/
        };

    for(var i = 0;i<newarr.length;i++){
        el = newarr[i];
        if(inv_orig[i].location == 'Nationwide')
        	is_nationwide = 'is_red';
        else
        	is_nationwide = '';
        if(el.indexOf(',') != -1){
			newlocations = el.split(',');
			$(newlocations).each(function(index, element) {
				element = element.trim();
				if(typeof locs.Locations[element] != "undefined"){
			        locs.Locations[element].lat += offset;
			        locs.Locations[element].lng += offset;
			        offset += 0.00005;

			        mar_map = new google.maps.Marker({
			                      position: locs.Locations[element],
			                      map: map,
			                      title: arr[i]+' : '+freq[newarr[i]]+' Investments',
			                      animation: google.maps.Animation.DROP,
			                      icon: mapicons[invstatus[i]].icon
			                    });
			        info_map = new google.maps.InfoWindow({
			                content: '<b style="font-weight:bold"><div><img class="mapinfologo" src="/'+ getInvesteeLogo(inv_orig[i].name.toLowerCase().replace(/ /g,'').replace('&','')) +'" alt="Logo"></div><p>REF CODE : '+inv_orig[i].refcode+'</p><p>INVESTEE NAME : '+inv_orig[i].name+'</p><p>INVESTMENT TITLE : '+inv_orig[i].title+'</p><p>INVESTMENT THEME : '+inv_orig[i].theme+'</p><p>LOCATION : '+inv_orig[i].location+'</p><p class="'+ is_nationwide +'">CONTRACT VALUE : $'+formatNum(inv_orig[i].contract)+'</p><a href="javascript:void(0)" data-index="'+i+'" class="view-more-map" style="float:right;color:#005591">View more info</a></b>'
			            });
			        markers.push(mar_map);
			        infowindows.push(info_map);
		        }
		        else{
		        	console.log(element + ' ,is undefined');
		        }
			});
		}
		else{
			el = el.trim();
			if(typeof locs.Locations[el] != "undefined"){
		        locs.Locations[el].lat += offset;
		        locs.Locations[el].lng += offset;
		        offset += 0.00005;

		        mar_map = new google.maps.Marker({
		                      position: locs.Locations[el],
		                      map: map,
		                      title: arr[i]+' : '+freq[newarr[i]]+' Investments',
		                      animation: google.maps.Animation.DROP,
		                      icon: mapicons[invstatus[i]].icon
		                    });
		                    //console.log(inv_orig[i]);
		        info_map = new google.maps.InfoWindow({
		                content: '<b style="font-weight:bold"><div><img class="mapinfologo" src="/'+ getInvesteeLogo(inv_orig[i].name.toLowerCase().replace(/ /g,'').replace('&','')) +'" alt="Logo"></div><p>REF CODE : '+inv_orig[i].refcode+'</p><p>INVESTEE NAME : '+inv_orig[i].name+'</p><p>INVESTMENT TITLE : '+inv_orig[i].title+'</p><p>INVESTMENT THEME : '+inv_orig[i].theme+'</p><p>LOCATION : '+inv_orig[i].location+'</p><p class="'+ is_nationwide +'">CONTRACT VALUE : $'+formatNum(inv_orig[i].contract)+'</p><a href="javascript:void(0)" data-index="'+i+'" class="view-more-map" style="float:right;color:#005591">View more info</a></b>'
		            });

		        markers.push(mar_map);
			    infowindows.push(info_map);
	        }
	        else{
		        	console.log(el + ' is undefined');
		        }
		}

    }

    var maplegend = document.getElementById('maplegend');
    $('#maplegend').append('<div><a href="javascript:void(0)" onclick="javascript:filterMapStatus(&quot;all&quot;);">ALL STATUSES</a></div>');
	for (var key in mapicons) {
	  if(key != 'terminated'){
	    var type = mapicons[key];
	    var name = type.name;
	    var icon = type.icon;
	    var div = document.createElement('div');
	    div.innerHTML = '<img src="' + icon + '"> <a href="javascript:void(0)" onclick="javascript:filterMapStatus(&quot;'+ name +'&quot;);">' + name + '</a>';
	    maplegend.appendChild(div);
	  }
	}
	var maplegend1 = document.getElementById('maplegend1');
	scores = '';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;1&quot;);"><50%</a>';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;2&quot;);">50%-59%</a>';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;3&quot;);">60%-69%</a>';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;4&quot;);">70%-79%</a>';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;5&quot;);">80%-90%</a>';
	scores += '<a class="riskscores" href="javascript:void(0)" onclick="javascript:filterRiskScore(this,&quot;6&quot;);">>90%</a>';
	scores += '<p>Updated as of<br/>Dec 2015</p>';
	var div1 = document.createElement('div');
	div1.innerHTML = scores;
	//console.log(maplegend1);
	maplegend1.appendChild(div1);
	$('#maplegend,#maplegend1').show();
	map.controls[google.maps.ControlPosition.LEFT_TOP].push(maplegend);
	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(maplegend1);
    //console.log(inv_orig);
    $('#donor-wrapper').off().on('click','.view-more-map',function(){
    	var index = parseInt($(this).attr('data-index'));
    	//console.log(index);
    	$('.cd-panel .invrefcode').text(inv_orig[index].refcode);
    	$('.cd-panel .invlogoimg').attr('src','/' + getInvesteeLogo(inv_orig[index].name.toLowerCase().replace(/ /g,'').replace('&','')));
    	$('.cd-panel .invname').text(inv_orig[index].name);
    	$('.cd-panel .invname').text(inv_orig[index].name);
    	$('.cd-panel .invstitle').text(inv_orig[index].title);
    	$('.cd-panel .invtheme').text(inv_orig[index].theme);
    	$('.cd-panel .invlocation').text(inv_orig[index].location);
    	$('.cd-panel .invdate').text(moment(inv_orig[index].startdate).format('Do MMMM YYYY'));
    	if(inv_orig[index].location == 'Nationwide')
        	$('.cd-panel .invamount').addClass('is_red');
    	$('.cd-panel .invamount').text('$'+formatNum(inv_orig[index].contract));
    	// var docs = alasql('SELECT * FROM ? WHERE refcode = "'+ inv_orig[index].refcode.trim() +'"',[inv_documents]);
    // 	if(docs.length == 0){
    // 		$('.cd-panel .doc-wrapper').html('<div class="col-md-12"><p style="margin-top:5px;">No Documents Found</p></div>');
    // 	}
    // 	else{
    // 		var content = '<div class="col-md-12" style="margin-top:20px">';
    // 		var filetype;
    // 		$(docs).each(function(index,value){
    // 			filetype = getFileType(value.url.substr(value.url.lastIndexOf('.')+1));
    // 			if(index % 2 == 0){
		//             content += '<div class="row margin-bottom">';
		//           }
		//           content += '<div class="col-md-5 ssf-document margin-right column-height">';
		//           if(supported_file_types.indexOf(filetype) > -1)
    // 					content += '<div class="row full-height is_file"><a class="doc-link" href = "/sites/stats/_layouts/15/WopiFrame.aspx?sourcedoc='+ value.url +'&action=default" target="_blank">';
  	// 			  else
    // 					content += '<div class="row full-height is_file"><a class="doc-link" href = "'+ value.url +'" target="_blank">'
		//           content += '<div class="col-md-5 full-height doc-'+ filetype +'"></div>';
		// 		  content += '<div class="col-md-7 full-height doc-body"><span class="file-title">'+value.name+'</span></div>';
    //
		//           content += '</a></div>';
		//           content += '</div>';
    //
		//          if(index -1 % 2 == 0){
		//             content += '</div>';
		//           }
		//          else if($(docs).length - 1 == index){
		//             content += "</div>";
		//           }
    //
    // 		});
    // 		content += '</div>';
    // 		$('.cd-panel .doc-wrapper').html(content);
    // 	}
    //
    // 	var images = alasql('SELECT * FROM ? WHERE refcode = "'+ inv_orig[index].refcode.trim() +'"',[inv_images]);
		// if(images.length == 0){
		// 	$('.cd-panel .image-wrapper').html('<div class="col-md-12"><p style="margin-top:5px;">No Images Found</p></div>');
		// }
		// else{
		// 	var content1 = '<div class="col-md-12" style="margin-top:20px">';
		// 	var filetype;
		// 	$(images).each(function(index,value){
		// 		if(index % 3 == 0){
		//             content1 += '<div class="row margin-bottom">';
		//           }
		//           content1 += '<div class="col-md-4 image-height">';
		//           content1 += '<a class="inv_image_link" data-lightbox="investment images" data-title="'+ value.name +'" href = "'+ value.url +'" >';
		//           content1 += '<img class="inv_image" src="'+ value.url +'" alt="'+ value.name +'">';
		//           content1 += '</a>';
		//           content1 += '</div>';
    //
		//          if(index - 2 % 3 == 0){
		//             content1 += '</div>';
		//           }
		//          else if($(images).length - 1 == index){
		//             content1 += "</div>";
		//           }
    //
		// 	});
		// 	content1 += '</div>';
		// 	$('.cd-panel .image-wrapper').html(content1);
		// }

    	$(".cd-panel-content").scroller('destroy');
    	$(".cd-panel-content").scroller();
       	$('.cd-panel').addClass('is-visible');
    });
    $('#donor-wrapper').on('click','.cd-panel-close',function(){
       	$('.cd-panel').removeClass('is-visible');
    });

    for(var j=0;j<markers.length;j++){
      google.maps.event.addListener(markers[j], 'click', function(innerKey) {
          return function() {
              closeAllInfoWindows();
              infowindows[innerKey].open(map, markers[innerKey]);
          }
        }(j));
    }

}

function closeAllInfoWindows() {
  for (var i=0;i<infowindows.length;i++) {
     infowindows[i].close();
  }
}



function handleClicks() {
  $('.is_folder').off().on('click', function(){
        folder_name = $(this).find('.file-title').text();
        inv_folder_path.push(folder_name);
        $('.breadcrumbs a').removeClass('active');
        $('.breadcrumbs').append('<a href="javascript:void(0)" class="navigator active" id="'+ folder_name +'">'+ folder_name +'</a>');
        getDocuments();
  });

  $('.navigator').off().on('click', function(){
	  $('.breadcrumbs a').removeClass('active');
	  $(this).addClass('active');
	  if($(this).hasClass('doc-home')){
	    $('.document-wrapper').empty();
	    setLoader();
	    inv_folder_path.length = 0;
	    $('.breadcrumbs a').not(':first').remove();
	    $('.loadDocs').click();
	  }
	  else if($(this).hasClass('list-level')){
	    $(this).nextAll().remove();
	    inv_folder_path.length = 0;
	    getDocuments();
	  }
	  else{
	    index = parseInt(inv_folder_path.indexOf($(this).attr('id'))) + 1;
	    inv_folder_path = inv_folder_path.slice(0,index);
	    $(this).nextAll().remove();
	    getDocuments();
	  }
	});

}


function formatNumber(x){
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
//
// function getInvMatrixData(){
// 	var content = '';
// 	$(investments_data).each(function(index,value) {
// 		content = '';
// 		content += '<tr>';
//   		content += '<td>' + value.refcode + '</td>';
//   		content += '<td>' + (value.approach || '') + '</td>';
//   		content += '<td>' + (value.name || '') + '</td>';
//   		content += '<td>' + (value.location || '') + '</td>';
//   		content += '<td>' + (formatNumber(value.contract) || '') + '</td>';
//   		content += '<td>' + (formatNumber(value.dec15) || '') + '</td>';
//   		content += '<td>' + (value.status || '') + '</td>';
//   		//content += '<td>' + (value.im || '') + '</td>';
//   		content += '</tr>';
//   		$('#investment-table tbody').append(content);
// 	});
// 	var inv_table = $('#investment-table').dataTable({
// 			        "autoWidth" : false,
// 			        "processing": true,
// 			   		"columnDefs": [
// 					    { "width": "10%", "targets": 0 },
// 					    { "width": "10%", "targets": 1 },
// 					    { "width": "10%", "targets": 2 },
// 					    { "width": "25%", "targets": 3 },
// 					    //{ "width": "13%", "targets": 4 },
// 					    { "width": "20%", "targets": 4 },
// 					    { "width": "15%", "targets": 5 },
// 					    { "width": "10%", "targets": 6 },
// 					    //{ "width": "20%", "targets": 7 },
// 					    //{ "width": "10%", "targets": 9 }
// 					  ],
// 					"lengthMenu": [[8,10], [8,10]],
// 					"iDisplayLength": 8,
// 					"language": {
// 		            "lengthMenu": "Display _MENU_ projects per page",
// 		            "zeroRecords": "No project found - with your search",
// 		            "info": "Showing page _PAGE_ of _PAGES_",
// 		            "infoEmpty": "No projects available",
// 		            "infoFiltered": "(filtered from _MAX_ total projects)"
// 		          }
// 			    });
//     inv_table.on('search.dt',function(){
//     	var api = inv_table.api();
//     	if(api.search() == "")
//     		$('.inv-files-small').show();
//     	else{
//     		$('.inv-files-small').hide();
//     		$('.inv-files-small .file-ref-small:containsi("'+ api.search() +'")').closest('.inv-files-small').show();
//     	}
//     	if($('.inv-files-small .file-ref-small').filter(':visible').length == 0)
//     		$('.inv-documents-wrapper .scroller-content').prepend('<p class="no-doc-results">No documents match your search</p>');
//     	else
//     		$('.inv-documents-wrapper .scroller-content .no-doc-results').remove()
//     	$('.inv-documents-wrapper').scroller("destroy");
//     	$('.inv-documents-wrapper').scroller();
//     	resizeSidebar();
//     });
//     inv_table.on( 'length.dt', function ( e, settings, len ) {
// 	    resizeSidebar();
// 	});
//     $('#investment-table tbody').off().on( 'click', 'tr', function () {
//         if ( $(this).hasClass('selected') ) {
//             $(this).removeClass('selected');
//         }
//         else {
//              $(this).addClass('selected');
//         }
//     });
//     $('.inv-table').removeClass('div-loading');
//     $(".inv-table").scroller({
//     	horizontal: true
//     });
//     resizeSidebar();
// }
// function getInvMatrixDocs(){
// 	$('.inv-documents-wrapper').empty();
// 	$().SPServices({
// 		operation: "GetListItems",
// 		webURL: window.location.origin + '/inv',
// 		async: true,
// 		listName: "Documents (Inv.)",
// 		CAMLViewFields: "<ViewFields><FieldRef Name='Investment'/><FieldRef Name='BaseName'/><FieldRef Name='EncodedAbsUrl'/><FieldRef Name='File_x0020_Type'/><FieldRef Name='File_x0020_Size'/></ViewFields>",
// 		completefunc: function (xData, Status) {
// 	  		var doc_array = $(xData.responseXML).SPFilterNode("z:row");
// 	  		var content = '';
// 	  		var filetype;
// 	  		$(doc_array).each(function() {
// 	  				file_type = getFileType($(this).attr("ows_File_x0020_Type"));
// 		  			content += '<div class="inv-files-small">';
// 		  			if(supported_file_types.indexOf(file_type) > -1)
// 						content += '<a class="kb-doc-link" href = "'+window.location.origin+'/sites/stats/_layouts/15/WopiFrame.aspx?sourcedoc='+ $(this).attr("ows_EncodedAbsUrl") +'&action=default" target="_blank">';
// 					else
// 						content += '<a class="kb-doc-link" href = "'+ $(this).attr("ows_EncodedAbsUrl")+'" target="_blank">';
// 		  			content += '<div class ="file-type-small kb-doc-'+ file_type +'"></div>';
// 		  			content += '</a>';
// 		  			content += '<div class="file-info-small">';
// 		  			content += '<div class = "file-ref-small"> Ref : '+ getColumnName($(this).attr("ows_Investment")) +'</div><br/>';
// 		  			content += '<div class = "file-name-small">'+ $(this).attr("ows_BaseName") +'</div>';
// 		  			//content += '<div class = "file-size-small">'+ getFileSize($(this).attr("ows_File_x0020_Size").split(";#")[1]) +'</div>';
// 		  			content += '</div>';
// 		  			content += '</div>';
// 	  		});
// 	  		$('.inv-documents-wrapper').removeClass('div-loading');
// 	  		$('.inv-documents-wrapper').append(content);
// 	  		$('.inv-documents-wrapper').scroller();
// 	  		resizeSidebar();
// 	    }
// 	});
// }
