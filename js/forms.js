//var siteUrl = 'https://somaliastabilityke-public.sharepoint.com/';
var siteUrl = '/';
var oListItem;


var appWebUrl = siteUrl.substring(0,siteUrl.length-1);
var ready = false;
var docLibrary = "Application Documents";
var file_paths = [];





function UploadFile() {



	$('.files-upload').each(function(){
		var fileInput = $(this);
		// if we couldnt get a reference to the file input then console.log the user and return
		if (!fileInput) {
			console.log('Oooooops... An error occured processing your input.');
			return true;
		}
		else if (!fileInput[0].files) {
			console.log("Oooooops... It doesn't look like your browse supports uploading files in this way");
			return true;
		}
		else if (fileInput[0].files.length <= 0) {
			console.log("Oooooops it doesn't look like you selected a file. Please select a file and try again.");
			return true;
		}
		else if(fileInput.val()==''){
			return true;
		}

		var file = fileInput[0].files[0];
		var file_name_with_extension = file.name;
		var file_name = file_name_with_extension.substring(0,file_name_with_extension.lastIndexOf('.'));
		var file_extension = file_name_with_extension.split('.').pop();
		var now = new Date();
		var file_name = file_name + '-' + now.format('dd-mm-yy_hh-mm-ss') + '.' + file_extension;
		file_paths.push(siteUrl + 'Application%20Documents/' + file_name);
		ProcessUpload(file_name,file, docLibrary, '');
	});

	insertApplyForm();

}

function ProcessUpload(file_name,fileInput, docLibraryName, folderName) {
	var reader = new FileReader();
	reader.onload = function (result) {
		var fileName = '',
		libraryName = '',
		fileData = '';

		var byteArray = new Uint8Array(result.target.result)
		for (var i = 0; i < byteArray.byteLength; i++) {
			fileData += String.fromCharCode(byteArray[i])
		}

		// once we have the file perform the actual upload
		PerformUpload(docLibraryName, file_name, folderName, fileData);

	};
	reader.readAsArrayBuffer(fileInput);
}

function PerformUpload(libraryName, fileName, folderName, fileData) {
	var url;

	// if there is no folder name then just upload to the root folder
	if (folderName == "") {
		url = appWebUrl + "/_api/web/lists/getByTitle(@TargetLibrary)/RootFolder/Files/add(url=@TargetFileName,overwrite='true')?" +
		"&@TargetLibrary='" + libraryName + "'" +
		"&@TargetFileName='" + fileName + "'";
	}
	else {
		// if there is a folder name then upload into this folder
		url = appWebUrl + "/_api/web/lists/getByTitle(@TargetLibrary)/RootFolder/folders(@TargetFolderName)/files/add(url=@TargetFileName,overwrite='true')?" +
		"&@TargetLibrary='" + libraryName + "'" +
		"&@TargetFolderName='" + folderName + "'" +
		"&@TargetFileName='" + fileName + "'";
	}

	// use the request executor (cross domain library) to perform the upload
	var reqExecutor = new SP.RequestExecutor(appWebUrl);
	reqExecutor.executeAsync({
		url: url,
		method: "POST",
		headers: {
			"Accept": "application/json; odata=verbose",
			"X-RequestDigest": digest
		},
		contentType: "application/json;odata=verbose",
		binaryStringRequestBody: true,
		body: fileData,
		success: function (x, y, z) {
			console.log("Success! Your file was uploaded to SharePoint.");
		},
		error: function (x, y, z) {
			console.log("Oooooops... it looks like something went wrong uploading your file " +z);
		}
	});
}



function insertApplyForm() {

	var clientContext = new SP.ClientContext(siteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Applications');

	var itemCreateInfo = new SP.ListItemCreationInformation();
	oListItem = oList.addItem(itemCreateInfo);

	oListItem.set_item('Title',$('#name').val());
	oListItem.set_item('EMail0',$('#email').val());
	oListItem.set_item('Investment_x0020_ID',$('#investment').val());
	for(var i=0;i<file_paths.length;i++)
	oListItem.set_item('File_x0020_'+(i+1), file_paths[i]);

	oListItem.update();

	clientContext.load(oListItem);

	clientContext.executeQueryAsync(Function.createDelegate(this, this.onInsertApplyFormSucceeded), Function.createDelegate(this, this.onInsertApplyFormFailed));
}

function onInsertApplyFormSucceeded() {

	console.log('Item created: ' + oListItem.get_id());
	$('.feedback-input').each(function(){
		$(this).val('');
	});
	$('.files-upload').each(function(){
		$(this).replaceWith($(this).clone(true));
	});

	showNotification("Your Form Has been Submitted,We Will get Back to You");
}

function onInsertApplyFormFailed(sender, args) {

	console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

var oCListItem;

function insertContactForm() {

	var clientContext = new SP.ClientContext(siteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Contact Us');

	var itemCreateInfo = new SP.ListItemCreationInformation();
	oCListItem = oList.addItem(itemCreateInfo);

	oCListItem.set_item('Name', $('#name').val());
	oCListItem.set_item('Email_x0020_Address', $('#email').val());
	oCListItem.set_item('Title', $('#subject').val());
	oCListItem.set_item('Message', $('#message').val());

	oCListItem.update();

	clientContext.load(oCListItem);

	clientContext.executeQueryAsync(Function.createDelegate(this, this.onInsertContactFormSucceeded), Function.createDelegate(this, this.onInsertContactFormFailed));
}

function onInsertContactFormSucceeded() {
	console.log('Item created: ' + oCListItem.get_id());
	$('.feedback-input').each(function(){
		$(this).val('');
	});
	showNotification('Your Form has been submitted, We Will contact you as soon as possible');
}

function onInsertContactFormFailed(sender, args) {

	console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}


var oSListItem;

function submitSubscription() {

	var clientContext = new SP.ClientContext(siteUrl);
	var oList = clientContext.get_web().get_lists().getByTitle('Site Subscription list');

	var itemCreateInfo = new SP.ListItemCreationInformation();
	oSListItem = oList.addItem(itemCreateInfo);

	oSListItem.set_item('E_x002d_mail', $('#subscribebox').val());
	oSListItem.update();

	clientContext.load(oSListItem);

	clientContext.executeQueryAsync(Function.createDelegate(this, this.onInsertSubscriptionSucceeded), Function.createDelegate(this, this.onInsertSubscriptionFailed));
}

function onInsertSubscriptionSucceeded() {
	console.log('Item created: ' + oSListItem.get_id());

	var my_call_back = 'jsonp_callback_' + Math.round(100000 * Math.random());
	var my_email = $('#subscribebox').val();
	var script = document.createElement('script');
	script.src = 'https://madmimi.com/signups/subscribe/115878.json?callback=' + my_call_back + '&signup[email]=' + my_email;
	document.body.appendChild(script);

	$('#subscribebox').val('')
	showNotification('You have successfully been added to the subscription list');
}

function onInsertSubscriptionFailed(sender, args) {

	console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}


function showNotification(the_message,error){
	error = typeof error !== 'undefined' ? error : false;
	var notification = new NotificationFx({
		message : '<p>'+the_message+'</p>',
		layout : 'growl',
		effect : 'scale',
		ttl:5000,
		type : 'notice', // notice, warning, error or success
		onClose : function() {

		}
	});

	// show the notification
	notification.show();

	if(error)
	$('.ns-effect-scale').css('background','red')
}

// (function($){
// 	$('form#application #button-blue').click(function(e){
// 		e.preventDefault();
// 		var errors = '';
// 		$('.feedback-input').each(function(){
// 			if($(this).val()=='')
// 			errors += $(this).attr('name')+' cannot be blank. \n';
// 		});
	// 	if(errors==''){
	// 		$('form#application #button-blue').unbind('click');
	// $('form#application').ajaxForm(function() {
	// 	showNotification("Thank you for your comment!");
	//
	// });
	// $('form#application').ajaxForm(
	// 	data:  $('form#frmjournal').serialize(),
	// 	dataType: 'json',
	// 	beforeSubmit: function(formData, jqForm, options) {
	// 		$('.feedback-input').each(function(){
	// 			if($(this).val()=='')
	// 			errors += $(this).attr('name')+' cannot be blank. \n';
	// 		});
	// 			if(errors!='')
	// 			showNotification('The following errors were encountered \n'+errors);
	// 		// optionally process data before submitting the form via AJAX
	// 	},
	// 	success : function(responseText, statusText, xhr, $form) {
	// 		// code that's executed when the request is processed successfully
	// 		showNotification('Success');
	// 	}
	// });

	// }else{
	// 	showNotification('The following errors were encountered \n'+errors);
	// }
	//});
// })( jQuery );
