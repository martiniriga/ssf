var locations;
var investments;
var investments_data;
var infowindows = [];
var markers = [];
var excelurl = myurl.template_url + '/js/excel.csv';
var logourl = myurl.template_url + '/images/logos/';
Papa.parse(excelurl,{
  download:true,
  header:true,
  complete:function(data){
    investments = data.data;
    investments_data = investments;
    locations = makeData(investments,'location');
    mapInvestments(locations,investments);
  }
});

Array.prototype.frequencies  = function () {
  var freqs = {sum: 0};
  this.map( function (a){
    if (!(a in this)) { this[a] = 1; }
    else { this[a] += 1; }
    this.sum += 1;
    return a; }, freqs
  );
  return freqs;
}

function makeData(arr,index){
  return $.map(arr,function(val,i){return val[''+index]})
}

function filterMapStatus(status){
  var query = '';
  if(status == 'all'){
    query = '';
  }
  else{
    if(status.indexOf('/') == '-1'){
      query = 'WHERE status = "' + status + '"';
    }
    else{
      var sep = status.split('/');
      query = 'WHERE status = "' + sep[0] + '" OR status = "' + sep[1] + '"';
    }
  }
  var sql =  'SELECT * from ? ' + query;
  var inv = alasql(sql,[investments_data]);

  locations = makeData(inv,'location');

  clearContent();

  mapInvestments(locations,inv);

}


function mapInvestments(arr, inv_orig){
  var myLatLng = {lat: 5.105, lng: 44.976};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: myLatLng,
    minZoom: 5
  });

  google.maps.event.addListener(map, "click", function(event) {
    closeAllInfoWindows();
  });
  google.maps.event.addListenerOnce(map, 'idle', function(){
    $('#map > div > div:nth-child(2),#map > div > div:nth-child(4),#map > div > div.gmnoprint.gm-style-cc').hide();
  });

  google.maps.event.addDomListener(window, "resize", function() {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  });

  var invstatus = $.map(makeData(inv_orig,'status'),function(val,i){return val.toLowerCase().replace(' ','').replace('-','')});


  locs = {
    "Locations": {
      "Baidoa": {
        "lat": 3.1140502,
        "lng": 43.651925000000006
      },
      "Balcad": {
        "lat": 2.3583953,
        "lng": 45.386288899999954
      },
      "Jowhar": {
        "lat": 2.7774231,
        "lng": 45.5016253
      },
      "Mogadishu": {
        "lat": 2.0469343,
        "lng": 45.31816230000004
      },
      "Kismayo": {
        "lat": -0.3560455,
        "lng": 42.54605730000003
      },
      "Beletweyne": {
        "lat": 4.7429412,
        "lng": 45.2009362
      },
      "Hargeisa": {
        "lat": 9.562389,
        "lng": 44.07701339999994
      },
      "Garowe": {
        "lat": 8.4084158,
        "lng": 48.48372389999997
      },
      "Borome": {
        "lat": 9.936009199999999,
        "lng": 43.18440199999998
      },
      "Burao": {
        "lat": 9.525916600000002,
        "lng": 45.53463069999998
      },
      "Galkayo": {
        "lat": 6.7872726,
        "lng": 47.439235199999985
      },
      "Abudwak": {
        "lat": 6.1481089,
        "lng": 46.397270400000025
      },
      "Bossaso": {
        "lat": 11.2755407,
        "lng": 49.18789939999999
      },
      "Bari": {
        "lat": 10.1203847,
        "lng": 49.6911374
      },
      "Galkacyo": {
        "lat": 6.7872726,
        "lng": 47.439235199999985
      },
      "Adado": {
        "lat": 11.325,
        "lng": 48.65777779999996
      },
      "Afgooye": {
        "lat": 2.1426226,
        "lng": 45.116716699999984
      },
      "Badhan": {
        "lat": 10.7149388,
        "lng": 48.33647189999999
      },
      "Dhahar": {
        "lat": 9.750082299999999,
        "lng": 48.82213690000003
      },
      "Buuhoodle": {
        "lat": 8.2363238,
        "lng": 46.33848160000002
      },
      "Laascaanood": {
        "lat": 8.475987000000002,
        "lng": 47.365760300000034
      },
      "Taleex": {
        "lat": 9.1490522,
        "lng": 48.42003269999998
      },
      "Middle Jubba": {
        "lat": 2.0780488,
        "lng": 41.60118139999997
      },
      "Afmadow": {
        "lat": 0.5150745999999999,
        "lng": 42.075706099999934
      },
      "Garbaharey": {
        "lat": 3.3304072,
        "lng": 42.21897809999996
      },
      "Qardho": {
        "lat": 9.511598,
        "lng": 49.08676439999999
      },
      "Hiraan": {
        "lat": 4.321015,
        "lng": 45.29938620000007
      },
      "Middle Shabelle": {
        "lat": 2.9250247,
        "lng": 45.903968899999995
      },
      "Badhaadhe": {
        "lat": -0.793613,
        "lng": 41.441250999999966
      },
      "Dollow": {
        "lat": 4.164174800000001,
        "lng": 42.07941630000005
      },
      "Belethaawo": {
        "lat": 3.9239247,
        "lng": 41.874975500000005
      },
      "Luuq": {
        "lat": 3.8131083,
        "lng": 42.54605730000003
      },
      "Elwak": {
        "lat": 2.6861757,
        "lng": 41.441250999999966
      },
      "Ishkushuban": {
        "lat": 10.2822147,
        "lng": 50.233457199999975
      },
      "Towfiiq": {
        "lat": 2.0740103,
        "lng": 45.339567699999975
      },
      "CeelBuuh": {
        "lat": 4.683913500000001,
        "lng": 46.61986079999997
      },
      "Dhudhub": {
        "lat": 9.775555599999999,
        "lng": 44.87583330000007
      },
      "Rako Raxo": {
        "lat": 9.7836986,
        "lng": 49.728161099999966
      },
      "Xiriiro": {
        "lat": 9.9770898,
        "lng": 50.251489799999945
      },
      "Godo": {
        "lat": 3.5039227,
        "lng": 42.2362435
      },
      "bjiraan": {
        "lat": 4.321015,
        "lng": 45.29938620000007
      },
      "Wajid": {
        "lat": 3.809294,
        "lng": 43.2461055
      },
      "Dhusamreb": {
        "lat": 5.5380625,
        "lng": 46.38646719999997
      },
      "Xudur": {
        "lat": 4.2544385,
        "lng": 43.94367880000004
      },
      "Mahaday": {
        "lat": 2.9716241,
        "lng": 45.533451499999956
      },
      "Barawe": {
        "lat": 1.1161954,
        "lng": 44.03181619999998
      },
      "South West": {
        "lat": 5.152149,
        "lng": 46.19961599999999
      },
      "Lower Juba": {
        "lat": 0.224021,
        "lng": 41.60118139999997
      },
      "Talex": {
        "lat": 9.1490522,
        "lng": 48.42003269999998
      },
      "Las Canood": {
        "lat": 8.475987000000002,
        "lng": 47.365760300000034
      },
      "Erigavo": {
        "lat": 10.6149654,
        "lng": 47.365760300000034
      },
      "Middle Juba": {
        "lat": 2.0780488,
        "lng": 41.60118139999997
      },
      "Lower Jubba": {
        "lat": 0.224021,
        "lng": 41.60118139999997
      },
      "Dhusamareeb": {
        "lat": 5.5380625,
        "lng": 46.38646719999997
      },
      "Abudwaaq": {
        "lat": 6.0333868,
        "lng": 46.36246860000006
      },
      "Hobyo": {
        "lat": 5.3515922,
        "lng": 48.524975100000006
      },
      "Xaradheere": {
        "lat": 2.3390467,
        "lng": 42.288005899999916
      },
      "Balambale": {
        "lat": 8.5,
        "lng": 46.28333333333333
      },
      "Godobjiraan": {
        "lat": 7.283333333333333,
        "lng": 49.36666666666667
      },
      "Dangoroyo": {
        "lat": 8.72686,
        "lng": 49.3415985
      },
      "Warsheikh": {
        "lat": 2.4203501,
        "lng": 45.78545
      },
      "Harfo": {
        "lat": 5.8333302,
        "lng": 48.1833
      },
      "Haro": {
        "lat": 5.8333302,
        "lng": 48.1833
      },
      "Dhoobley": {
        "lat": 0.4112971,
        "lng": 41.00889440000003
      },
      "BeletHawa": {
        "lat": 3.9275473,
        "lng": 41.8581997
      },
      "Belet Hawa": {
        "lat": 3.9275473,
        "lng": 41.8581997
      },
      "MiddleJubba": {
        "lat": 2.0780488,
        "lng": 41.60118139999997
      },
      "MiddleJuba": {
        "lat": 2.0780488,
        "lng": 41.60118139999997
      },
      "Jubbaland": {
        "lat": 3.9318113,
        "lng": 41.87171680000006
      },
      "Jubaland": {
        "lat": 3.9318113,
        "lng": 41.87171680000006
      },
      "Baraawe": {
        "lat": 1.1161954,
        "lng": 44.03181619999998
      },
      "SouthWest": {
        "lat": 5.152149,
        "lng": 46.19961599999999
      },
      "LowerJubba": {
        "lat": 0.224021,
        "lng": 41.60118139999997
      },
      "LowerJuba": {
        "lat": 0.224021,
        "lng": 41.60118139999997
      },
      "Berdale": {
        "lat": 3.3516667,
        "lng": 44.12222220000001
      },
      "Nationwide":{
        "lat":4.670835,
        "lng":46.548332
      },
      "Nonspecific":{
        "lat":9.868190,
        "lng":49.376860
      },
    }
  }

  newarr = arr.slice(0);

  var freq = newarr.frequencies();
  var offset = 0.0002;
  markers = [];
  infowindows = [];
  var is_nationwide = "";
  var iconBase = 'https://maps.google.com/mapfiles/ms/micons/';

  var mapicons = {
    completed: {
      name: 'Completed/Terminated',
      icon: iconBase + 'red-dot.png'
    },
    terminated: {
      name: 'Completed/Terminated',
      icon: iconBase + 'red-dot.png'
    },
    contracted: {
      name: 'Contracted',
      icon: iconBase + 'green-dot.png'
    },

    committed: {
      name: 'Committed',
      icon: iconBase + 'ltblue-dot.png'
    },
    designphase: {
      name: 'Design phase',
      icon: iconBase + 'purple-dot.png'
    },
    pledged: {
      name: 'Pledged',
      icon: iconBase + 'orange.png'
    }

  };

  for(var i = 0;i<newarr.length;i++){
    el = newarr[i];

    if(inv_orig[i].location == 'Nationwide')
    is_nationwide = 'is_red';
    else
    is_nationwide = '';
    if(el.indexOf(',') != -1){
      newlocations = el.split(',');
      $(newlocations).each(function(index, element) {
        element = element.trim();
        if(typeof locs.Locations[element] != "undefined"){
          locs.Locations[element].lat += offset;
          locs.Locations[element].lng += offset;
          offset += 0.00005;

          mar_map = new google.maps.Marker({
            position: locs.Locations[element],
            map: map,
            title: arr[i]+' : '+freq[newarr[i]]+' Investments',
            animation: google.maps.Animation.DROP,
            icon: mapicons[invstatus[i]].icon
          });
          info_map = new google.maps.InfoWindow({
            content: '<b style="font-weight:bold"><div><img class="mapinfologo" src="'+logourl+inv_orig[i].name+'.png" alt="Logo"></div><p>REF CODE : '+inv_orig[i].refcode+'</p><p>INVESTEE NAME : '+inv_orig[i].name+'</p><p>INVESTMENT TITLE : '+inv_orig[i].title+'</p><p>INVESTMENT THEME : '+inv_orig[i].theme+'</p><p>LOCATION : '+inv_orig[i].location+'</p></b>'
          });
          markers.push(mar_map);
          infowindows.push(info_map);
        }
        else{
          console.log(element + ' ,is undefined');
        }
      });
    }
    else{
      el = el.trim();

      if(typeof locs.Locations[el] != "undefined"){
        locs.Locations[el].lat += offset;
        locs.Locations[el].lng += offset;
        offset += 0.00005;

        mar_map = new google.maps.Marker({
          position: locs.Locations[el],
          map: map,
          title: arr[i]+' : '+freq[newarr[i]]+' Investments',
          animation: google.maps.Animation.DROP,
          icon: mapicons[invstatus[i]].icon
        });

        info_map = new google.maps.InfoWindow({
          content: '<b style="font-weight:bold"><div><img class="mapinfologo" src="" alt="Logo"></div><p>REF CODE : '+inv_orig[i].refcode+'</p><p>INVESTEE NAME : '+inv_orig[i].name+'</p><p>INVESTMENT TITLE : '+inv_orig[i].title+'</p><p>INVESTMENT THEME : '+inv_orig[i].theme+'</p><p>LOCATION : '+inv_orig[i].location+'</p></b>'
        });

        markers.push(mar_map);
        infowindows.push(info_map);
      }
      else{
        console.log(el + ' is undefined');
      }
    }

  }

  var maplegend = document.getElementById('maplegend');

  $('#maplegend').append('<div><a href="javascript:void(0)" onclick="javascript:filterMapStatus(&quot;all&quot;);">ALL STATUSES</a></div>');
  for (var key in mapicons) {
    if(key != 'terminated'){
      var type = mapicons[key];
      var name = type.name;
      var icon = type.icon;
      var div = document.createElement('div');
      div.innerHTML = '<img src="' + icon + '"> <a href="javascript:void(0)" onclick="javascript:filterMapStatus(&quot;'+ name +'&quot;);">' + name + '</a>';
      maplegend.appendChild(div);
    }
  }

  $('#maplegend').show();
  map.controls[google.maps.ControlPosition.LEFT_TOP].push(maplegend);

  for(var j=0;j<markers.length;j++){
    google.maps.event.addListener(markers[j], 'click', function(innerKey) {
      return function() {
        closeAllInfoWindows();
        infowindows[innerKey].open(map, markers[innerKey]);
      }
    }(j));
  }

}

function closeAllInfoWindows() {
  for (var i=0;i<infowindows.length;i++) {
    infowindows[i].close();
  }
}
function clearContent(){
  jQuery('.content-section').empty().html("<style>#map{height:600px;width:100%;float:left}.mapinfologo{height:40px;display:block;margin:5px auto}#maplegend div,#maplegend h3{margin-bottom:10px}#maplegend{background:#fff;padding:10px;margin:10px;border:1px solid #000}#maplegend h3{font-size:15px;margin-top:0;font-weight:700}"
  + "</style>"+
  "<div id='map'></div><div id='maplegend'><h3>Legend</h3></div>");
}
