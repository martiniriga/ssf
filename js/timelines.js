jQuery(document).ready(function($) {
      var $timeline_block = $('.cd-timeline-block');
      var firstelm = $timeline_block.first();

        //on scolling, show/animate timeline blocks when enter the viewport
        $(window).on('scroll', function() {
          var _win = $(window), iselm = null;
          $timeline_block.each(function(index) {
            var _this = $(this);
            if (((_this.offset().top - _win.height())) <= (_win.scrollTop())) {
              iselm = _this;
            }

          });
          if (_win.scrollTop() < $(firstelm).offset().top) {
            iselm = $(firstelm);
          }

          if (iselm) {
           $('.cd-active').removeClass('cd-active').addClass('cd-inactive');
              iselm.find('.cd-timeline-img').removeClass('cd-inactive').addClass('cd-active');

            if ((iselm.offset().top - _win.height()) > (_win.scrollTop() * 0.75)) {
              $('.cd-date').removeClass('cd-ssf-color');
            }
            iselm.find('.cd-date').addClass('cd-ssf-color');
          }
        });
      });
