q<?php get_header();
$pageposts = $wpdb->get_results("SELECT * from wp_submissions");

?>


	    <?php if ( $pageposts : ?>
				<?php global $post ?>
	    <?php foreach ($pageposts as $post): ?>
			<div>
			<?php if (has_post_thumbnail()): ?>
				<a href="/ssf/wp-content/uploads/2016/02/<?php echo $pageposts->file1;?>" class="feed-post-thumbnail col-1-4"><?php the_permalink() ?><?php the_post_thumbnail(); ?></a>
			<?php endforeach; ?>
			<p><?php the_content(); ?></p>


	    <?php endwhile; ?>
	    <?php else : ?>
        <div>
	        <h2>Nothing Found</h2>
	        <p>Sorry but what you are looking for is not here</p>
	        <p><a href="<?php echo get_option('home'); ?>"></a></p>
        </div>
	    <?php endif; ?>



<?php get_footer(); ?>
