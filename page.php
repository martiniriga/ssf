<?php get_header(); ?>


	    <?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post();?>
			<div>
			<?php if (has_post_thumbnail()): ?>
				<a href="<?php the_permalink() ?>" class="img-responsive"><?php the_post_thumbnail(); ?></a>
			<?php endif; ?>
			<p><?php the_content(); ?></p>


	    <?php endwhile; ?>
	    <?php else : ?>
        <div>
	        <h2>Nothing Found</h2>
	        <p style="color: #0072C6;">The page you're looking for doesn't exist.</p>
	        <p>Check for a typo in the URL, or <a href="<?php echo get_option('home'); ?>">go to the site home</a></p>
        </div>
	    <?php endif; ?>



<?php get_footer(); ?>
