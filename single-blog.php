<tr>
  <?php if ( have_posts() ) : ?>
    <td valign="top">
      <ul class="ms-blog-postList">
        <?php while ( have_posts() ) : the_post();?>
          <li>
            <div class="ms-blog-postBox ms-shadow">
              <div class="ms-blog-postBoxMargin">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <br>
                <div class="ms-blog-postBody">
                  <?php the_content(); ?>                  
                </div>
                <p></p>
              </div>
            </div>
            <div class="ms-blog-postDivider"></div>
          </li>
        </ul>
      </td>
      <td valign="top" class="ms-blog-LeftColumn">
          <div id="blog">
        <?php if ( in_category( 'blog' ) ) : ?>
            <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
              <aside id="sidebar" role="complementary">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
              </aside><!-- .widget-area -->
            <?php endif; ?>
       <?php else : ?>
           <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
              <aside id="sidebar" role="complementary">
                <?php dynamic_sidebar( 'sidebar-4' ); ?>
              </aside><!-- .widget-area -->
            <?php endif; ?>
      <?php endif; ?>
          </div>
      </td>
    </tr>


  <?php endwhile; ?>
<?php else : ?>
  <h2>Nothing Found</h2>
  <p>Sorry could not find the blog post your looking for</p>
  <p><a href="<?php echo get_option('home'); ?>"></a></p>
<?php endif; ?>
