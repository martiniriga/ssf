
	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<div class="wrapper-news clearfix">
				<!-- posts list -->
				<div id="posts-list" class="clearfix">

					<div class="entry-date"><div class="month"><?php the_time('F'); ?></div> <div class="year"><?php the_time('Y'); ?></div> <em></em> </div>
					<article class="format-standard news-item" style="display: block;">
						<div class="post-heading"><h4 class="post-title"><?php the_title(); ?></h4></div>
						<div class="excerpt"><?php the_content(); ?></div>
						<div class="feedback">
    					<?php if ( comments_open() || get_comments_number() ) :
    						comments_template();
    					endif; ?>
    					</div>
							<?php if ( in_category( 'newsletter' ) ) : ?>
								<a href="<?php echo $_SERVER['HTTP_REFERER'] ?>" style="text-decoration:none"><div class="back-news"><i class="fa fa-back fa-arrow-left"></i><span class="back-news-span">Back to Newsletters</span></div></a>
							<?php else : ?>
							<a href="<?php echo $_SERVER['HTTP_REFERER'] ?>" style="text-decoration:none"><div class="back-news"><i class="fa fa-back fa-arrow-left"></i><span class="back-news-span">diib Warsidaha</span></div></a>
							<?php endif; ?>

					</article>

				</div>
				<!-- ENDS posts list -->
				<?php if ( in_category( 'newsletter' ) ) : ?>
					 <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		            <aside id="sidebar" role="complementary">
		                <?php dynamic_sidebar( 'sidebar-1' ); ?>
		            </aside><!-- .widget-area -->
		      <?php endif; ?>
				<?php else : ?>
					<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
							 <aside id="sidebar" role="complementary">
									 <?php dynamic_sidebar( 'sidebar-2' ); ?>
							 </aside><!-- .widget-area -->
				 <?php endif; ?>
				<?php endif; ?>
				<!-- Fold image -->
				<div id="fold">
				</div>
			</div>

		<?php endwhile; ?>

	<?php else : ?>

		<h2>Nothing Found</h2>
		<p>Sorry but what you are looking for is not here</p>
		<p><a href="<?php echo get_option('home'); ?>"></a></p>

	<?php endif; ?>
