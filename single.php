<?php get_header(); ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/feedback-form.css">
<span id="DeltaPlaceHolderMain"> 
<?php if(in_category('newsletter') || in_category('newsletter_so')): ?>
<div id="main-news">	
<?php endif; ?> 

    <table cellpadding="0" cellspacing="0" border="0" width="95%">
        <tbody>
        	<tr>
            	<td>
                	<table cellpadding="0" cellspacing="0" class="ms-blog-MainArea">
                    	<tbody>  
<?php
if(in_category('blog') || in_category('blog_so')):

	get_template_part('single-blog');
   
elseif(in_category('newsletter') || in_category('newsletter_so')):

	get_template_part('single-newsletter');	

else : ?>

	<h2>Nothing Found</h2>
	<p>Sorry but what you are looking for is not here</p>
	<p><a href="<?php echo get_option('home'); ?>"></a></p>

<?php endif; ?> 
						</tbody>                  
                	</table>
	            </td>
	        </tr>
	    </tbody>
    </table>  
<?php if(in_category('newsletter') || in_category('newsletter_so')): ?>
</div>	
<?php endif; ?>    
</span>
<?php get_footer();?>
